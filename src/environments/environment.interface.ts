export interface EnvironmentInterface {
    name: 'dev' | 'stage' | 'prod';
    production: boolean;
    logApi: boolean;
    enableProfiler: boolean,
    serverUrl: string;
    userAPIUrl: string;
    dataAPIUrl: string;
}