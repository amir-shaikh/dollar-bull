import { EnvironmentInterface } from "./environment.interface";

export const environment:EnvironmentInterface = {
  name: 'dev',
  production: false,
  logApi: true,
  enableProfiler: true,
  serverUrl: 'http://18.188.151.234:4000',
  userAPIUrl: 'http://18.188.151.234:4000/user/v1',
  dataAPIUrl: 'http://18.188.151.234:4001/data/v1',
};
