import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../@shared/shared.module';
import { KycRoutingModule } from './kyc-routing.module';
import { KycComponent } from './kyc.component';


@NgModule({
  declarations: [KycComponent],
  imports: [
    CommonModule,
    KycRoutingModule,
    SharedModule,
  ]
})
export class KycModule { }
