import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../@core/service/auth/auth.service';
import { CommonService } from '../@core/service/common/common.service';
import { FingerprintService } from '../@core/service/fingerprint/fingerprint.service';
import { StocksService } from '../@core/service/stocks/stocks.service';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.component.html',
})
export class SplashComponent implements OnInit {
  constructor(private router: Router, private authService: AuthService, private commonService: CommonService, private stocksService: StocksService, private fingerprintService: FingerprintService) { }

  ngOnInit(): void {
    if(this.fingerprintService.getScreenLockPermission()){
      this.showFingerprint()
    }else{
      this.handleAuthentication();
    }
  }

  showFingerprint(){
    if(this.fingerprintService.getIsFingerAuthenticated()){
      this.handleAuthentication();
      return;
    }

    this.fingerprintService.isAvailable().then(() => {

      this.fingerprintService.show("Hey User, please verify your finger").then(r => {
        this.handleAuthentication();
      }).catch(e => {
        this.showFingerprint();
      })
    }).catch(e => {
      alert('fingerprint not available')
    })
  }

  handleAuthentication():void {
    const isAuthenticated = this.authService.isAuthenticated();

    if (!isAuthenticated) {
      this.signUpNavigation();
    } else {
      this.handlePreloadApis();
    }

    // this.handleInitialNavigation();
  }

  signUpNavigation() {
    this.router.navigate(['/signup'], { replaceUrl: true });
  }

  handleInitialNavigation() {
    this.router.navigate(['/dashboard'], { replaceUrl: true });
  }

  async handlePreloadApis() {
    const isGuestUser = this.authService.isGuestUser();

    // Load apis whose data is required on initial navigation screens
    await this.commonService.getDollarbullStats().toPromise();
    await this.stocksService.getTopGainerLoserStocks({
      limit: 20,
      offset: 0
    }).toPromise();

    if (!isGuestUser) {
      // Load apis whose data is required
      // on initial navigation screens if user is not guest

    }

    this.handleInitialNavigation();
  }
}
