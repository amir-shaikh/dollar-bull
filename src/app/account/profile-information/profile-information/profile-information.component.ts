import { Component, OnInit } from '@angular/core';
import { JwtService } from 'src/app/@core/service/jwt.service';

@Component({
  selector: 'app-profile-information',
  templateUrl: './profile-information.component.html'
})
export class ProfileInformationComponent implements OnInit {
userData:any;
  constructor(
    private jwtService: JwtService) { }

  ngOnInit(): void {
    this.userData = this.jwtService.readTokenData();
  }

}
