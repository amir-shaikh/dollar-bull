import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileInformationComponent } from './profile-information/profile-information.component';

const routes: Routes = [{ path: '', component: ProfileInformationComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileInformationRoutingModule { }
