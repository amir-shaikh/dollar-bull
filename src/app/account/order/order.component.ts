import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute  } from '@angular/router';
import { StocksService } from 'src/app/@core/service/stocks/stocks.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  companyId:string ="";
  limit:string = '5';
  offset:string = '1';
  orderListData =[];
  pendingOrderListData =[];
  perPage= 5;
  pendingPage= 1;
  executedPage= 1;
  pendingTotalCount= 0;
  executedTotalCount= 0;
  loading= true;
  showLoadBtn = true;
  orderTabs: any[] = [
    {
      link: 'Executed',
      label: 'Executed',
      preventDefault: true,
    },
    {
      link: 'Pending',
      label: 'Pending',
      preventDefault: true,
    },
  ]
  activeFinancialTab: string = 'Executed';
  constructor( private router: Router,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private stockService: StocksService,) { }

  ngOnInit(): void {
    let obj ={
      perPage : this.limit,
      page :this.offset,


    }
    this.stockService.getOrderList(obj).subscribe(res => {
      this.orderListData = res.records;
      this.executedTotalCount = res.totalCount;
      this.handleLoadBtn( this.orderListData,res.totalCount)
    this.loading=false;

    })
    
    this.stockService.pendingOrder(obj).subscribe(res=>{
      this.pendingOrderListData = res.records;
      this.pendingTotalCount = res.totalCount;
    this.loading=false;

    })
  }
  handleOnClick(e: any) {
    console.log('tabs',e.item.link)
    this.activeFinancialTab = e.item.link;
    if (this.activeFinancialTab == "Pending") {
      this.handleLoadBtn( this.pendingOrderListData,this.pendingTotalCount)
    }else{
      this.handleLoadBtn( this.orderListData,this.executedTotalCount)
    }
  }

  handleLoadBtn(list,totalCount){
    if(list.length < totalCount){
      this.showLoadBtn=true;
    }else{
      this.showLoadBtn=false;

    }
  }

  loadMore(){
    this.loading=true;
    if (this.activeFinancialTab == "Pending") {
      this.loadMorePending();
    }else{
      this.loadMoreExecuted();
    }
 }

 loadMorePending(){
  this.pendingPage =this.pendingPage+1
  this.stockService.pendingOrder({perPage:this.perPage, page:this.pendingPage}).subscribe((res)=>{
    this.pendingOrderListData = [...this.pendingOrderListData, ...res.records];
    this.handleLoadBtn(this.pendingOrderListData,res.totalCount);
      this.pendingTotalCount = res.totalCount;
      this.loading=false;
  })
  }
  loadMoreExecuted(){
    this.executedPage =this.executedPage+1
  this.stockService.getOrderList({perPage:this.perPage, page:this.executedPage}).subscribe((res)=>{
    this.loading=false;
    this.orderListData = [...this.orderListData, ...res.records];
      this.executedTotalCount = res.totalCount;
      this.handleLoadBtn(this.orderListData,res.totalCount);
  })
  }
  cancelOrder($event,orderId){
    $event.stopPropagation();
    this.stockService.cancelOrder(orderId).subscribe((success)=>{
     let message ="Order Canceled";
      this.toastr.success(message,"",{timeOut: 3000,disableTimeOut:false,});

      this.stockService.pendingOrder({perPage:this.pendingOrderListData.length, page:1}).subscribe((res)=>{
        this.pendingOrderListData = res.records;
        this.handleLoadBtn(this.pendingOrderListData,res.totalCount);
          this.pendingTotalCount = res.totalCount;
          this.loading=false;
      })
    },
    fail=>{
      let message =  fail.error && fail.error.message ? fail.error.message :"Failed To Cancel order";
      this.toastr.error(message,"",{timeOut: 3000,disableTimeOut:false,});
          console.error(fail);
      
    })
  }

}
