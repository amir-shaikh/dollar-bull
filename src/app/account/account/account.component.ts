import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/@core/service/auth/auth.service';
import { NavigationService } from 'src/app/@core/service/navigation.service';
import { JwtService } from 'src/app/@core/service/jwt.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
})
export class AccountComponent implements OnInit {
  isAuthenticated: boolean = false;
  isGuestUser: boolean = false;
  accountCards: any[] = [
    {
      title: 'Profile Information',
      info: 'Information Complete',
      iconPath: 'assets/icons/icon-profile.png',
      navigateTo: `profile-information`,
      allowedNoGuestAuth: false,
    },
    {
      title: 'Talk to Us',
      info: 'Submit queries',
      iconPath: 'assets/icons/icon-help.png',
      navigateTo: `need-help`,
      allowedNoGuestAuth: true,
    },
    {
      title: ' Funds',
      info: 'Manage your wallet and bank account',
      iconPath: 'assets/icons/icon-wallet.png',
      navigateTo: `wallet`,
      allowedNoGuestAuth: false,
    },
    {
      title: 'Orders',
      info: 'See successful, failed & pending transactions',
      iconPath: 'assets/icons/icon-order.png',
      navigateTo: 'order',
      allowedNoGuestAuth: true,
    },
    {
      title: 'News and Articles',
      info: 'Read news and articles related to investment',
      iconPath: 'assets/icons/icon-article.png',
      navigateTo: `news-articles`,
      allowedNoGuestAuth: true,
    },
    // {
    //   title: 'US Stock account',
    //   info: 'Account details',
    //   iconPath: 'assets/icons/icon-user-add.png',
    //   navigateTo: 'us-stock-account',
    //   allowedNoGuestAuth: false,
    // },
    // {
    //   title: 'Risk Profile',
    //   info: 'Check your risk profile now.',
    //   iconPath: 'assets/icons/icon-meter.png',
    //   navigateTo: `risk-profile`,
    //   allowedNoGuestAuth: false,
    // },
    {
      title: 'Dollarbull safe and secure',
      info: 'Your data is safe with us',
      iconPath: 'assets/icons/icon-verified.png',
      navigateTo: `safe-secure`,
      allowedNoGuestAuth: true,
    },

    {
      title: 'Manage Permissions',
      info: 'Secure your account on Dollarbull app.',
      iconPath: 'assets/icons/icon-fingerprint.png',
      navigateTo: `manage-permissions`,
      allowedNoGuestAuth: false,
    },
    {
      title: 'More...',
      info: 'View terms & conditions , privacy policy, disclaimer',
      iconPath: 'assets/icons/icon-more.png',
      navigateTo: 'more',
      allowedNoGuestAuth: true,
    },
  ];
  displayAccountCards: any[] = [];
  userData:any;

  constructor(
    private navigationService: NavigationService,
    private authService: AuthService,
    private jwtService: JwtService
  ) {}

  ngOnInit(): void {
    this.isGuestUser = this.authService.isGuestUser();
    this.isAuthenticated = this.authService.isAuthenticated();
    this.userData = this.jwtService.readTokenData();console.log(this.userData);
    this.displayAccountCards = this.accountCards.filter((card: any) => {
      // display all cards for auth but non-guest user
      if (!this.isGuestUser) {
        return true;
      }

      // display allowed cards to non-guest user
      if (card.allowedNoGuestAuth) {
        return true;
      }

      return false;
    });
  }

  handleLogout() {
    this.authService.logout().subscribe((res) => {
      this.navigationService.stackFirst(['/splash']);
    });
  }

  handleLogin() {
    this.authService.authenticate('1').subscribe((res) => {
      this.navigationService.stackFirst(['/splash']);
    });
  }
}
