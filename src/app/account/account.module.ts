import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountRoutingModule } from './account-routing.module';
import { AccountComponent } from './account/account.component';
import { SharedModule } from '../@shared/shared.module';
import { NewsArticlesComponent } from './news-articles/news-articles.component';
import { AccountCardComponent } from './components/accountcard/account-card/account-card.component';
import { ManagePermissionComponent } from './manage-permission/manage-permission.component';
import { NeedHelpComponent } from './need-help/need-help.component';
import { SafeSecureComponent } from './safe-secure/safe-secure.component';
import { InfoCardComponent } from './components/info-card/info-card.component';
import { RiskProfileComponent } from './risk-profile/risk-profile.component';
import { WalletComponent } from './wallet/wallet.component';
import { OrderComponent } from './order/order.component';
import { ArticleComponent } from './article/article.component';
import { MoreComponent } from './more/more.component';
import { TermsConditionComponent } from './terms-condition/terms-condition.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { SecurityComponent } from './security/security.component';
import { GoogleApiDisclosureComponent } from './google-api-disclosure/google-api-disclosure.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'


@NgModule({
  declarations: [AccountComponent, AccountCardComponent, NewsArticlesComponent, ManagePermissionComponent, NeedHelpComponent, SafeSecureComponent, InfoCardComponent, RiskProfileComponent, WalletComponent, OrderComponent, ArticleComponent, MoreComponent, TermsConditionComponent, PrivacyPolicyComponent, SecurityComponent, GoogleApiDisclosureComponent],
  imports: [
    CommonModule,
    RouterModule,
    AccountRoutingModule,    
    FormsModule, 
    ReactiveFormsModule,
    SharedModule
  ],
  exports: [NewsArticlesComponent, AccountCardComponent, ManagePermissionComponent, InfoCardComponent ,OrderComponent, MoreComponent, TermsConditionComponent, PrivacyPolicyComponent, SecurityComponent, GoogleApiDisclosureComponent,]
})
export class AccountModule { }
