import { TermsConditionComponent } from './terms-condition/terms-condition.component';
import { MoreComponent } from './more/more.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsArticlesComponent } from './news-articles/news-articles.component';
import { AccountComponent } from './account/account.component';
import { ManagePermissionComponent } from './manage-permission/manage-permission.component';
import { NeedHelpComponent } from './need-help/need-help.component';
import { SafeSecureComponent } from './safe-secure/safe-secure.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { SecurityComponent } from './security/security.component';
import { GoogleApiDisclosureComponent } from './google-api-disclosure/google-api-disclosure.component';

import { OrderComponent } from './order/order.component';

import { RiskProfileComponent } from './risk-profile/risk-profile.component';
import { WalletComponent } from './wallet/wallet.component';
import { ArticleComponent } from './article/article.component';

import { AuthGuard } from '../@core/guard/auth.guard';
import { NoGuestAuthGuard } from '../@core/guard/guest-auth.guard';

const routes: Routes = [
  {
    path: '',
    component: AccountComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'profile-information',
    loadChildren: () =>
      import('./profile-information/profile-information.module').then(
        (m) => m.ProfileInformationModule
      ),
    canActivate: [NoGuestAuthGuard],
    data: {
      noGuestGuardRedirectUrl: '/account',
    }
  },
  {
    path: 'manage-permissions',
    component: ManagePermissionComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'need-help',
    component: NeedHelpComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'safe-secure',
    component: SafeSecureComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'order',
    component: OrderComponent,
    canActivate: [AuthGuard],
    // data: {
    //   noGuestGuardRedirectUrl: '/account',
    // }
  },
  {
    path: 'risk-profile',
    component: RiskProfileComponent,
    canActivate: [NoGuestAuthGuard],
    data: {
      noGuestGuardRedirectUrl: '/account',
    }
  },
  {
    path: 'wallet',
    component: WalletComponent,
    canActivate: [NoGuestAuthGuard],
    data: {
      noGuestGuardRedirectUrl: '/account',
    }
  },
  {
    path: 'more',
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: MoreComponent,
      },
      {
        path: 'terms-condition',
        component: TermsConditionComponent,
      },
      {
        path: 'privacy-policy',
        component: PrivacyPolicyComponent,
      },
      {
        path: 'security',
        component: SecurityComponent,
      },
      {
        path: 'google-api-disclosure',
        component: GoogleApiDisclosureComponent,
      },
      {
        path: '**',
        redirectTo: '',
      },
    ],
  },
  {
    path: 'news-articles',
    component: NewsArticlesComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'news-articles/:id',
    component: ArticleComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountRoutingModule {}
