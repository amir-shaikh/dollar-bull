import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-more',
  templateUrl: './more.component.html',
  styleUrls: ['./more.component.scss']
})
export class MoreComponent implements OnInit {
  accountCards = [
    { title: "Terms & Conditions",  iconPath: "assets/icons/tc.svg", navigateTo: `terms-condition`, ld: true },
    { title: "Privacy Policy",iconPath: "assets/icons/pc.svg", navigateTo: `privacy-policy`, ld: true },
    { title: "Security", iconPath: "assets/icons/lock.svg", navigateTo: "security", ld: true }
  ];
  constructor() { }

  ngOnInit(): void {
    
  }

}
