import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-safe-secure',
  templateUrl: './safe-secure.component.html'
})
export class SafeSecureComponent implements OnInit {

  constructor() { }

  infoCards = [
    { title: "Sec-registered - compliance.", 
    // info: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus elementum molestie nibh at vehicula.", 
    iconPath: "assets/icons/register-complaint.svg" },
    { title: "Encryption", 
    // info: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus elementum molestie nibh at vehicula.", 
    iconPath: "assets/icons/encr.svg" },
    { title: "Insurance", 
    // info: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus elementum molestie nibh at vehicula.",
     iconPath: "assets/icons/insurance.svg" },
    { title: "Data practices / safety", 
    // info: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus elementum molestie nibh at vehicula.",
     iconPath: "assets/icons/data-pract.svg" },
    // { title: "Reputed and experienced team.", info: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus elementum molestie nibh at vehicula.", iconPath: "assets/icons/icon-user.png" },

  ]

  ngOnInit(): void {
  }

}
