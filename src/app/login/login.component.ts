import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormsModule, Validators } from '@angular/forms';
import { SignUpService } from '../@core/service/signup/signup.service';
import { Router } from '@angular/router';
import { JwtService } from '../@core/service/jwt.service';
// import { SocialAuthService } from "angularx-social-login";
// import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { AuthService } from 'src/app/@core/service/auth/auth.service';
import {registerWebPlugin} from "@capacitor/core";
import {OAuth2Client} from '@byteowls/capacitor-oauth2';
import {
  Plugins
} from '@capacitor/core';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {

  enterOTPSection: boolean = false;
  emailForm: any;
  otpForm: any;
  enterEmail: boolean = true;
  app_login_msg: string = "";
  otp_screen_msg: string = "";
  constructor(private fb: FormBuilder, private signUpService: SignUpService, private router: Router, private jwtService: JwtService, 
    // private socailAuthService: SocialAuthService,
     private authService: AuthService) { }

  ngOnInit(): void {
    registerWebPlugin(OAuth2Client);
    this.emailForm = this.fb.group({
      email_id: ['', [Validators.required, Validators.email]],
    });
    this.otpForm = this.fb.group({
      otpNumber: ['', [Validators.required, Validators.maxLength(6)]],
    })
  }

  app_login() {
    console.log(this.emailForm.value);
    if (this.emailForm.valid) {
      let data = {
        email_id: this.emailForm.get('email_id').value,
        login_type: 'Normal',
      }
      this.signUpService.app_login(data).subscribe((res) => {
        console.log(["res", res]);
        this.enterEmail = false;
        this.enterOTPSection = true;
      },
        (err) => {
          this.app_login_msg = err.error.message;
          console.log(err);
        }
      );


    }
  }

  resendOTP() {
    console.log(this.otpForm.value);
    let data = {
      email_id: this.emailForm.get('email_id').value,
      login_type: 'Normal',
    }
    this.signUpService.app_login(data).subscribe((res) => {
      let response_msg: any = res;
      this.otp_screen_msg = response_msg.message;
    },
      (err) => {
        this.otp_screen_msg = err.error.message;
        console.log(err);
      }
    );
  }

  onEmailOTPSubmit(event: any) {
    let email_otp = event.target.value;
    event.target.selectionEnd = 5;
    if (email_otp.length == 6) {
      let data = {
        email_id: this.emailForm.get('email_id').value,
        email_code: this.otpForm.get('otpNumber').value,
        unique_id: ''
      }
      this.signUpService.onEmailOTPSubmitLogin(data).subscribe((res) => {
        console.log(["res", res]);
        let response: any = res
        this.otp_screen_msg = "";
        this.jwtService.saveToken(response.result.token);
        this.authService.saveHasInvested(true);
        this.router.navigate(['/dashboard'], { replaceUrl: true });
      },
        (err) => {
          console.log(err)
          this.otp_screen_msg = err.error.message;
        }
      );
    }
  }

  

  signInWithGoogle(): void {
 let oauth2Options = {
  authorizationBaseUrl: "https://accounts.google.com/o/oauth2/auth",
  accessTokenEndpoint: "https://www.googleapis.com/oauth2/v4/token",
  scope: "email profile",
  resourceUrl: "https://www.googleapis.com/userinfo/v2/me",
  web: {
    appId: "27218086515-nv298gnqprilfjlchhb741su2aa98qga.apps.googleusercontent.com",
    responseType: "token", // implicit flow
    accessTokenEndpoint: "", // clear the tokenEndpoint as we know that implicit flow gets the accessToken from the authorizationRequest
    redirectUrl: "http://localhost:4200",
    windowOptions: "height=600,left=0,top=0"
  },
  android: {
    appId: "27218086515-i5lv43ik1ars5b6s6d0044oilpbbavk7.apps.googleusercontent.com",
    responseType: "code", // if you configured a android app in google dev console the value must be "code"
    redirectUrl: "com.dollarbull" // package name from google dev console
  },
  ios: {
    appId: "27218086515-i5lv43ik1ars5b6s6d0044oilpbbavk7.apps.googleusercontent.com",
    responseType: "code", // if you configured a ios app in google dev console the value must be "code"
    redirectUrl: "com.dollarbull" // Bundle ID from google dev console
  }
};
    Plugins.OAuth2Client.authenticate(
      oauth2Options
  ).then(response => {
      // let accessToken = response["access_token"];
      // this.refreshToken = response["refresh_token"];

      // only if you include a resourceUrl protected user values are included in the response!
      this.processSocialSignup(response,"GOOGLE");

      console.log(response)

      // go to backend
  }).catch(reason => {
      console.error("OAuth rejected", reason);
  });

    // this.socailAuthService.signIn(GoogleLoginProvider.PROVIDER_ID).then((data)=>{
    //   console.log(data)
    //   this.processSocialSignup(data,"GOOGLE");
    // }).catch(err=>console.log(err));
  }

  signInWithFB(): void {
    // this.socailAuthService.signIn(FacebookLoginProvider.PROVIDER_ID).then((data)=>{
    //   console.log(data)
    //   this.processSocialSignup(data,"FACEBOOK");
    // }).catch(err=>console.log(err));
  }

  processSocialSignup(data,loginType){
    let loginData = {
      email_id: data.email,
      email_code: "",
      // login_type : loginType,
      unique_id : data.id
    }
    this.signUpService.onEmailOTPSubmitLogin(loginData).subscribe((res) => {
      console.log(["res", res]);
      let response: any = res
      this.otp_screen_msg = "";
      this.jwtService.saveToken(response.result.token);
      if (response.result.is_invested == 1) {
        this.authService.saveHasInvested(true);
      }
      else{
        this.authService.saveHasInvested(false);
      }
      this.authService.saveHasInvested(true);
      this.router.navigate(['/dashboard'], { replaceUrl: true });
    },
      (err) => {
        console.log(err)
        this.otp_screen_msg = err.error.message;
      }
    );
  }

  signOut(): void {
    // this.socailAuthService.signOut();
  }
}
