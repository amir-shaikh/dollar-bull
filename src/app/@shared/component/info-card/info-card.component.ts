import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-info-card',
  templateUrl: './info-card.component.html',
  styleUrls: ['./info-card.component.scss']
})
export class InfoCardComponent implements OnInit {
  isPositive: string = "true"
  @Input()
  name :string = "Amazon com Inc" ;

  @Input() symbol :string = "TSLA" ;

  @Input()
  img :string = "https://storage.googleapis.com/iexcloud-hl37opg/api/logos/IEI.png";

  @Input()
  price :any;

  @Input() companyId :any = 1;

  @Input()
  company :string = "" ;

  @Input()
  stats :any;

  @Input()
  logo :any;

  @Input() previous_price : any = 0;
  constructor(private router: Router) { }

  ngOnInit(): void {
    this.name = (this.name) ? this.name : "Amazon com Inc";
    this.img = (this.img) ? this.img : "assets/icons/amazon-65-675861.webp";
    this.company = (this.company) ? this.company : "Sector";
    if(this.price){
      // console.log(["this.price", this.price, "this.previous_price", this.previous_price]);
      
      let price_diff = this.price - this.previous_price;
      if (price_diff > 0) {
        this.isPositive = "true"
      }
      else{
        this.isPositive = "false"
      }
      let percentage_val = 0;
      if (this.previous_price > 0) {
        percentage_val = (this.price - this.previous_price)/ this.previous_price*100;
      }
      this.stats = "$" + price_diff.toFixed(2) + "(" + percentage_val.toFixed(2) + "%)";
    }
   
  }

  handleClick(){
    if(!this.companyId){ return; }

    this.router.navigate(['explore/stocks/'+this.companyId])
  }

}
