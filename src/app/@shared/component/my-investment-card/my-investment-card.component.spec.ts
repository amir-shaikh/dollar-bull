import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyInvestmentCardComponent } from './my-investment-card.component';

describe('MyInvestmentCardComponent', () => {
  let component: MyInvestmentCardComponent;
  let fixture: ComponentFixture<MyInvestmentCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyInvestmentCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyInvestmentCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
