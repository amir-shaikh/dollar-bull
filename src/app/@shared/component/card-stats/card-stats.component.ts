import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-stats',
  templateUrl: './card-stats.component.html',
  styleUrls: ['./card-stats.component.scss']
})
export class CardStatsComponent implements OnInit {
  @Input() stats: any = "0"
  @Input() is_positive: string = "true";


  img : string = "assets/icons/triangle.svg" 
  constructor() { }
  ngOnInit(): void {
    if (this.is_positive == "true") {
      this.img = "assets/icons/triangle.svg";
    }
    else {
      this.img = "assets/icons/triangle_down.svg"
    }
  }

}
