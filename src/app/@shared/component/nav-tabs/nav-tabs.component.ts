import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-nav-tabs',
  templateUrl: './nav-tabs.component.html',
})
export class NavTabsComponent implements OnInit {
  @Input()
  tabs: TabsI[] = [];

  @Input()
  activeClass: string = 'active';

  @Output()
  onClick: EventEmitter<any> = new EventEmitter();

  activeIndex: number = 0;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    const activeLink = this.router.url;
    console.log(['activeLink', activeLink]);
    // if (this.tabs.length > 0) {
    //   this.activeIndex = 0;
    // }
    this.activeIndex = this.tabs.findIndex((item) => item.link && item.link === activeLink);
    if (this.activeIndex == -1) {
      this.activeIndex = 0;
    }
  }
  // ngOnDestroy(){
  //   this.activeIndex = 0;
  // }


  

  handleClick(e: any, item: TabsI, index: number) {
    e.preventDefault();
    console.log(["item",item]);
    
    this.activeIndex = index;

    this.onClick.emit({item, index});

    if (item.preventDefault) {
      return;
    }
    
    if (item.link) {
    this.router.navigate([item.link], { replaceUrl: true });
    }
  }
  // TODO: logic to keep active tab within viewport on click
}

export interface TabsI {
  link?: string;
  label: string;
  icon?:string;
  preventDefault?: boolean;
}
