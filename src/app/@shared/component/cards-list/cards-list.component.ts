import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cards-list',
  templateUrl: './cards-list.component.html',
  styleUrls: ['./cards-list.component.scss']
})
export class CardsListComponent implements OnInit {

  @Input()
  title: any = "";
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  handleViewAll(){
    this.router.navigate(['explore/stocks/all-stocks'])
  }

}
