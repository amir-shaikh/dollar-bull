import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartTooltipOptions } from "chart.js";
import { Color, Label } from "ng2-charts";

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnInit {
  
  @Output() handleMonthPerformance: EventEmitter<any> = new EventEmitter();

  @Input()
  label: Array<any> = []

  @Input()
  dataSets: Array<{
    data: number[],
    label: string
  }> = [
      {
        data: [],
        label: ""
      }
    ]

  @Input()
  months: Array<any> = []

  

  dataLoaded: boolean = false
  monthsInCharts: any = []
  constructor() { }

  ngOnInit(): void {
    setTimeout(() => {
      this.lineChartLabels = this.label
      this.lineChartData = this.dataSets
      this.monthsInCharts = this.months
      this.dataLoaded = true
    }, 1000)
  }

  public lineChartData: ChartDataSets[] = this.dataSets;

  public lineChartLabels: Label[] = this.label;

  public lineChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      gridLines: {
        display: false
      }
    }
  };

  public lineChartColors: Color[] = [
    {
      backgroundColor: '#3B64BEBA',
    }
  ];

  public lineChartLegend = true;
  public lineChartType = "line" as const;
  public lineChartPlugins = [];

  handleParentMonth(month:any){
    this.handleMonthPerformance.emit(month)
  }
}

