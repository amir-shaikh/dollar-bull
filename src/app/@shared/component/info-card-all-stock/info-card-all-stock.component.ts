import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-info-card-all-stock',
  templateUrl: './info-card-all-stock.component.html',
  styleUrls: ['./info-card-all-stock.component.scss']
})
export class InfoCardAllStockComponent implements OnInit {
  is_positive:boolean = true;
  @Input()
  name:string="Amazon com Inc" ;
  @Input()
  logo:string ="assets/icons/amazon-65-675861.webp";
  @Input()
  capType:string ="Mid Cap"
  @Input()
  symbol:string="APPL";
  @Input()
  price:string="$214.25";
  @Input()
  dailyChange:any
  @Input()
  dailyChangeStatusImage:string="assets/icons/triangle.svg";
  @Input()
  marketCap:string="126.5T";
  @Input()
  ratio:string="126.5T";
  @Input()
  companyId = 1;
  @Input()
  sector: string = "IT";

  constructor( private router: Router,) { }

  ngOnInit(): void {
    if(this.dailyChange){
      this.dailyChange = parseFloat(this.dailyChange)
      if(this.dailyChange > 0){
        this.is_positive = true
      } else{
        this.is_positive = false
      }
    } 
  }
  handleClick(){
    this.router.navigate([`explore/stocks/${this.companyId}`])
  }

}
