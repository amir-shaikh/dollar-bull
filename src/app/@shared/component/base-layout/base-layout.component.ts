import { TabsI } from './../nav-tabs/nav-tabs.component';
import { Component, Input, OnInit, Output, EventEmitter, TemplateRef } from '@angular/core';
import { NavigationService } from 'src/app/@core/service/navigation.service';
import { CommonService } from 'src/app/@core/service/common/common.service';
import { AuthService } from 'src/app/@core/service/auth/auth.service';

@Component({
  selector: 'app-base-layout',
  templateUrl: './base-layout.component.html',
})
export class BaseLayoutComponent implements OnInit {
  hasInvested:boolean= false;

  @Input()
  enableMobileMenu: boolean = false;

  @Input()
  mobileHeaderDashBoard :boolean=false;

  @Input()
  enableMobileHeader: boolean = false;
  
  @Input()
  enableDeskTopHeader: boolean = false;

  @Input()
  enableDeskTopHeaderAction: boolean = false;

  @Input()
  title: string = '';

  @Input()
  disableDefaultBack: boolean = false;

  @Output()
  onBackClick: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  // @ts-ignore
  headerActionTemplate: TemplateRef<any>;

  desktopNavTabs: TabsI[] = [
    {
      link: '/explore/stocks',
      label: 'Explore',
    },
    {
      link: '/dashboard',
      label: 'Dashboard',
    },
    {
      link: '/account',
      label: 'Account',
    },
  ];

  loaderEnabled: any = false;

  constructor(private navigation: NavigationService, private commonService: CommonService, private authService: AuthService) {}

  ngOnInit(): void {
    this.commonService.isLoaderEnabled$.subscribe(loaderEnabled => {
      this.loaderEnabled = loaderEnabled
    });
    let tmp =this.authService.getHasInvested();
    if (tmp) {
      this.hasInvested = true;
    }
    else{
      this.hasInvested = false;
    }
  }

  handleBackClick(e: any) {
    if (this.onBackClick) {
      this.onBackClick.emit(e);
    }

    if (this.disableDefaultBack) {
      return;
    }
    this.navigation.back();
  }
}
