import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { WebviewService } from '../../../@core/service/webview/webview.service'
import { CommonService } from '../../../@core/service/common/common.service'
import { NgbModalConfig, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-become-dollarbull-card',
  templateUrl: './become-dollarbull-card.component.html',
  providers: [NgbModalConfig, NgbModal],

})
export class BecomeDollarbullCardComponent implements OnInit {
  @ViewChild("fundTransferModal") modalContent: TemplateRef<any>;
  @ViewChild("ComplteKycOptionsModal") kycOprionContent: TemplateRef<any>;
  @ViewChild("noteModal") noteContent: TemplateRef<any>;


  displayFundTransferCards: any[] = [
    {
      title: 'Remit funds online',
      info: 'You must fund your account from a bank account you own. You may not transfer funds from third party',
      iconPath: 'assets/icons/netbanking.svg',
      navigateTo: `netbanckingstep`,
    },
    {
      title: 'Remit funds through bank branch',
      info: 'You must fund your account from a bank account you own. You may not transfer funds from third party',
      iconPath: 'assets/icons/a2process.svg',
      navigateTo: `fundtransferbank`,
    },

  ];
  displayKycOptionCards: any[] = [
    {
      title: 'Open KYC',
      info: 'Start KYC / complete pending KYC',
      iconPath: 'assets/icons/kyc-icon-modal.svg',
      navigateTo: `netbanckingstep`,
    },
    {
      title: 'Login to Tradestation',
      info: 'Logi here if your KYC is verified',
      iconPath: 'assets/icons/trade-station-icon-modal.png',
      navigateTo: `fundtransferbank`,
    },
  ]
  openKYCStage: string = "pending";
  verifyingKYCStage: string = "pending";
  addFundsStage: string = "pending";
  actionBtnLabel: string = "";
  // content = document.getElementById('content');
  actionStage: number = 0;
  //1-submitKYC, 2-verifyingKYC, 3-addFunds

  constructor(config: NgbModalConfig, private webviewService: WebviewService, private commonService: CommonService, private router: Router, private modalService: NgbModal,) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit(): void {

    this.getCustomerLatestStage();
    // this.selectStage(1);
  }


  getCustomerLatestStage() {
    this.commonService.getCustomerLatestStage().subscribe((res) => {
      console.log(["res", res]);
      let response: any = res;
      this.selectStage(response.result.current_stage);
    },
      (err) => {
        console.log(err)
      }
    );
  }

  selectStage(stage: number) {
    this.actionStage = stage;
    switch (stage) {
      case 0:
        //submitKYC
        this.openKYCStage = "current";
        this.verifyingKYCStage = "pending";
        this.addFundsStage = "pending";
        this.actionBtnLabel = "Open KYC";
        break;
      case 1:
        //verifyingKYC
        //Check min funds of 2k with trade station
        this.openKYCStage = "done";
        this.verifyingKYCStage = "done";
        this.addFundsStage = "pending";
        this.actionBtnLabel = "Add Funds";
        break;
      case 2:
        // addFunds
        this.openKYCStage = "done";
        this.verifyingKYCStage = "done";
        this.addFundsStage = "done";
        this.actionBtnLabel = "Learn How to Trade";
        break;
      case 3:
        break;
      default:
        break;
    }
  }

  actionBtnClick(currentStage: number) {
    console.log(["currentStage", currentStage]);
    
    this.updateCustomerStage(currentStage);
  }

  updateCustomerStage(stageNo: number) {
    switch (stageNo) {
      case 0:
        this.modalService.open(this.kycOprionContent);
        break;
      case 1:
        this.modalService.open(this.noteContent, { centered: true });
        break;
      case 2:
        this.modalService.open(this.noteContent, { centered: true });
        break;
    }
    
  }

  navigateToFundus(val: any) {
    this.modalService.dismissAll();
    var urlsh = val;
    if (val != '') {
      this.router.navigate([`/dashboard/fundtransfer/${urlsh}`], { replaceUrl: true });
    }
  }

  redirectToKYCForm(){
    this.webviewService.open_kyc_link();
  }

  openLRSPopup(){
    this.modalService.dismissAll();
    this.modalService.open(this.modalContent);
  }

  redirectToLoginForm(){
  //redirect customer to Trade station login page
  this.modalService.dismissAll();
  let data = {
      "stage": 1
    }
    this.commonService.updateCustomerStage(data).subscribe((res) => {
     this.selectStage(1); 
    },
      (err) => {
        console.log(err)
      }
    );
}

}
