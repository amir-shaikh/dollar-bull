import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-price',
  templateUrl: './card-price.component.html',
  styleUrls: ['./card-price.component.scss']
})
export class CardPriceComponent implements OnInit {
  @Input() price : string = "0" 
  currency : string = "$" 

  constructor() { }

  ngOnInit(): void {
  }

}
