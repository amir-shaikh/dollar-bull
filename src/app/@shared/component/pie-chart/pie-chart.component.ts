import { Component, Input, OnInit } from '@angular/core';
import { ChartType, ChartOptions } from 'chart.js';

import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent implements OnInit {

  @Input()
  labelProps: Array<string> = []
  @Input()
  valueProps: Array<number> = []

  dataLoaded: boolean = false

  public pieChartOptions: ChartOptions = {
    responsive: true,
    // maintainAspectRatio: false,
    legend: {
      position: "right"
    },

  };

  public pieChartLabels: Label[] = [];
  public pieChartData: SingleDataSet = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public pieChartColors: Array<any> = [{
    backgroundColor: ['#3B64BE', '#88A1D7', '#7090D080'] // TODO: from config
  }]

  constructor() {
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
    
  }

  ngOnInit() {
    setTimeout(() => {
      this.pieChartLabels = this.labelProps
      this.pieChartData = this.valueProps
      this.dataLoaded = true
    }, 1000)
  }

}
