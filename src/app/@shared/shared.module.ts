import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
// shared components
import { LoaderComponent } from './component/loader/loader.component';
import { ExploreLayoutComponent } from './component/explore-layout/explore-layout.component';
import { SearchFieldComponent } from './component/search-field/search-field.component';
import { NavTabsComponent } from './component/nav-tabs/nav-tabs.component';
import { CardsListComponent } from './component/cards-list/cards-list.component';
import { InfoCardComponent } from './component/info-card/info-card.component';
import { BaseLayoutComponent } from './component/base-layout/base-layout.component';
import { CardPriceComponent } from './component/card-price/card-price.component';
import { CardStatsComponent } from './component/card-stats/card-stats.component';
import { NewsCardComponent } from './component/news-card/news-card.component';
import { NewsCardVerticalComponent } from './component/news-card-vertical/news-card-vertical.component';
import { ToggleComponent } from './component/toggle/toggle/toggle.component';
import { LineChartComponent } from './component/line-chart/line-chart.component';
import { PieChartComponent } from './component/pie-chart/pie-chart.component';
import { InfoCardAllStockComponent } from './component/info-card-all-stock/info-card-all-stock.component';
import { BecomeDollarbullCardComponent } from './component/become-dollarbull-card/become-dollarbull-card.component';
import { MarketOpenCardComponent } from './component/market-open-card/market-open-card.component';
import { MyInvestmentCardComponent } from './component/my-investment-card/my-investment-card.component';

// shared pipe
import { BaseTitlePipe } from './pipe/base-title.pipe';
import { SignCurrencyPipe } from './pipe/sign-currency.pipe';

@NgModule({
  declarations: [
    LoaderComponent,
    ExploreLayoutComponent,
    SearchFieldComponent,
    NavTabsComponent,
    CardsListComponent,
    InfoCardComponent,
    BaseLayoutComponent,
    CardPriceComponent,
    CardStatsComponent,
    BaseTitlePipe,
    NewsCardComponent,
    ToggleComponent,
    LineChartComponent,
    PieChartComponent,
    InfoCardAllStockComponent,
    NewsCardVerticalComponent,
    BecomeDollarbullCardComponent,
    MarketOpenCardComponent,
    MyInvestmentCardComponent,
    SignCurrencyPipe,
  ],
  imports: [CommonModule, RouterModule, ChartsModule],
  exports: [
    LoaderComponent,
    ExploreLayoutComponent,
    SearchFieldComponent,
    NavTabsComponent,
    CardsListComponent,
    InfoCardComponent,
    BaseLayoutComponent,
    CardPriceComponent,
    CardStatsComponent,
    BaseTitlePipe,
    NewsCardComponent,
    ToggleComponent,
    LineChartComponent,
    PieChartComponent,
    InfoCardAllStockComponent,
    NewsCardVerticalComponent,
    BecomeDollarbullCardComponent,
    MarketOpenCardComponent,
    MyInvestmentCardComponent,
    SignCurrencyPipe,
  ],
})
export class SharedModule {}
