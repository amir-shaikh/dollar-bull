import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormsModule, Validators } from '@angular/forms';
import { SignUpService } from '../../@core/service/signup/signup.service';
import { JwtService } from '../../@core/service/jwt.service';
import { Router } from '@angular/router';
import { SocialAuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import {registerWebPlugin} from "@capacitor/core";
import {OAuth2Client} from '@byteowls/capacitor-oauth2';
import {
  Plugins
} from '@capacitor/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
})
export class SignupComponent implements OnInit {
  partitioned: number;
  phoneNumberSection: boolean = false;
  enterEmailOTPSection: boolean = false;
  mobileOTPSection: boolean = false;
  personalDetailSection: boolean = true;
  enterRmCode: boolean = false;
  phoneNumberForm: any;
  emailOTPForm: any;
  mobileOTPForm: any;
  personalDetailForm: any;
  rmCodeForm: any;
  sign_up_msg: string = "";
  email_otp_msg: string = "";
  mobile_otp_msg:string = "";
  rmcode_msg:string="";
  enteredMobileNo : string = "";



  constructor(private fb: FormBuilder, private signUpService: SignUpService, private jwtService: JwtService,private router: Router, private socailAuthService: SocialAuthService) {

    registerWebPlugin(OAuth2Client);

  }

  ngOnInit(): void {
    
    this.personalDetailForm = this.fb.group({
      fullName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
    })
    this.emailOTPForm = this.fb.group({
      emailOTPNumber: ['', [Validators.required, Validators.maxLength(6)]],
    })

    this.mobileOTPForm = this.fb.group({
      mobileOTPNumber: ['', [Validators.required, Validators.maxLength(6)]],
    })
    this.phoneNumberForm = this.fb.group({
      phoneNumber: ['', [Validators.required, Validators.pattern('[1-9]{1}[0-9]{9}')]],
    });


    this.rmCodeForm = this.fb.group({
      rmcode: ['',],

    });
    // this.showScreen("phoneNumberSection");


  }

  enterYourDetail() {
    console.log(this.personalDetailForm);
    if (this.personalDetailForm.valid) {
      let data = {
        email_id: this.personalDetailForm.get('email').value,
        login_type: "Normal",
        name: this.personalDetailForm.get('fullName').value
      }
      this.signUpService.callSignupAPI(data).subscribe((res) => {
        console.log(["res", res]);
        this.sign_up_msg = "";
        let response: any = res
        this.jwtService.saveToken(response.result.token)
        this.showScreen('enterEmailOTPSection');
      },
        (err) => {
          console.log(err)
          this.sign_up_msg = err.error.message;
        }
      );
    }
  }

  signInWithGoogle(): void {
    let oauth2Options = {
      authorizationBaseUrl: "https://accounts.google.com/o/oauth2/auth",
      accessTokenEndpoint: "https://www.googleapis.com/oauth2/v4/token",
      scope: "email profile",
      resourceUrl: "https://www.googleapis.com/userinfo/v2/me",
      web: {
        appId: "27218086515-nv298gnqprilfjlchhb741su2aa98qga.apps.googleusercontent.com",
        responseType: "token", // implicit flow
        accessTokenEndpoint: "", // clear the tokenEndpoint as we know that implicit flow gets the accessToken from the authorizationRequest
        redirectUrl: "http://localhost:4200",
        windowOptions: "height=600,left=0,top=0"
      },
      android: {
        appId: "27218086515-i5lv43ik1ars5b6s6d0044oilpbbavk7.apps.googleusercontent.com",
        responseType: "code", // if you configured a android app in google dev console the value must be "code"
        redirectUrl: "com.dollarbull:/signup" // package name from google dev console
      },
      ios: {
        appId: "27218086515-i5lv43ik1ars5b6s6d0044oilpbbavk7.apps.googleusercontent.com",
        responseType: "code", // if you configured a ios app in google dev console the value must be "code"
        redirectUrl: "com.dollarbull:/" // Bundle ID from google dev console
      }
    };
        Plugins.OAuth2Client.authenticate(
          oauth2Options
      ).then(response => {
          // let accessToken = response["access_token"];
          // this.refreshToken = response["refresh_token"];
    
          // only if you include a resourceUrl protected user values are included in the response!
          this.processSocialSignup(response,"GOOGLE");
    
          console.log(response)
    
          // go to backend
      }).catch(reason => {
          console.error("OAuth rejected", reason);
      });
    // this.socailAuthService.signIn(GoogleLoginProvider.PROVIDER_ID).then((data)=>{
    //   console.log(data)
    //   this.processSocialSignup(data,"GOOGLE");
    // }).catch(err=>console.log(err));
  }

  signInWithFB(): void {
    this.socailAuthService.signIn(FacebookLoginProvider.PROVIDER_ID).then((data)=>{
      console.log(data)
      this.processSocialSignup(data,"FACEBOOK");
    }).catch(err=>console.log(err));
  }

  processSocialSignup(data,loginType){
    let signupData = {
      email_id : data.email,
      name : data.name,
      login_type : loginType,
      unique_id : data.id
    }
    this.signUpService.callSignupAPI(signupData).subscribe((res) => {
        console.log("res", res);
        this.sign_up_msg = "";
        let response : any = res
        let token =response.result.token
        this.jwtService.saveToken(token)
        this.showScreen('phoneNumberSection');
      },
      (err) => {
        console.log("err",err)
        this.sign_up_msg = err.error.message;
      }
    );
  }

  signOut(): void {
    this.socailAuthService.signOut();
  }

  onEmailOTPSubmit(event: any) {
    let email_otp = event.target.value;
    if (email_otp.length == 6) {
      event.target.selectionEnd = 5;
      let data = {
        email_code: this.emailOTPForm.get('emailOTPNumber').value
      }
      this.signUpService.onEmailOTPSubmit(data).subscribe((res) => {
        console.log(["res", res]);
        this.email_otp_msg = "";
        this.showScreen('phoneNumberSection');
      },
        (err) => {
          console.log(err)
          this.email_otp_msg = err.error.message;
        }
      );
    }
  }

  resendEmailOTP() {
    //Resend OTP to email id
    this.signUpService.resendEmailOTP().subscribe((res) => {
      console.log(["res", res]);
      let responseData:any = res
      this.email_otp_msg = responseData.message;
    },
      (err) => {
        console.log(err)
        this.email_otp_msg = err.error.message;
      }
    );
  }

  resendMobileOTP() {
    //Resend OTP to email id
    this.signUpService.resendMobileOTP().subscribe((res) => {
      console.log(["res", res]);
      let responseData:any = res
      this.mobile_otp_msg = responseData.message;
    },
      (err) => {
        console.log(err)
        this.mobile_otp_msg = err.error.message;
      }
    );
  }

  showScreen(name) {
    switch (name) {
      case "enterEmailOTPSection":
        this.enterEmailOTPSection = true;
        this.personalDetailSection = false;
        this.phoneNumberSection = false;
        this.mobileOTPSection = false;
        this.enterRmCode = false;
        break;
      case "personalDetailSection":
        this.enterEmailOTPSection = false;
        this.personalDetailSection = true;
        this.phoneNumberSection = false;
        this.mobileOTPSection = false;
        this.enterRmCode = false;
        break;
      case "phoneNumberSection":
        this.enterEmailOTPSection = false;
        this.personalDetailSection = false;
        this.phoneNumberSection = true;
        this.mobileOTPSection = false;
        this.enterRmCode = false;
        break;
      case "mobileOTPSection":
        this.enterEmailOTPSection = false;
        this.personalDetailSection = false;
        this.phoneNumberSection = false;
        this.mobileOTPSection = true;
        this.enterRmCode = false;
        break;
      case "enterRmCode":
        this.enterEmailOTPSection = false;
        this.personalDetailSection = false;
        this.phoneNumberSection = false;
        this.mobileOTPSection = false;
        this.enterRmCode = true;
        break;
      default:
        break;
    }
  }

  updateMobileNo() {
    if (this.phoneNumberForm.valid) {
      this.enteredMobileNo = this.phoneNumberForm.get('phoneNumber').value.toString();
      let data = {
        mobile_no: this.enteredMobileNo
      }
      this.signUpService.updateMobileNo(data).subscribe((res) => {
        console.log(["res", res]);
        this.showScreen("mobileOTPSection");
      },
        (err) => {
          console.log(err);
        }
      );
    }
  }

  verifyMobileOTP(event: any) {
    let mobile_otp = event.target.value;
    if (mobile_otp.length == 6) {
      event.target.selectionEnd = 5;
      let data = {
        otp: this.mobileOTPForm.get('mobileOTPNumber').value
      }
      this.signUpService.onMobileOTPSubmit(data).subscribe((res) => {
        console.log(["res", res]);
        this.mobile_otp_msg = "";
        this.showScreen("enterRmCode");
      },
        (err) => {
          console.log(err)
          this.mobile_otp_msg = err.error.message;
        }
      );
    }
  }

  enterCodeNext() {
    if (this.rmCodeForm.valid) {
      this.showScreen('enterRmCode');
    }
  }

  mobileContinue() {
    if (this.phoneNumberForm.valid) {
      this.showScreen('enterRmCode');
    }
  }

  skipMobileNumber() {
    this.showScreen('enterRmCode');
  }

  submitRMCode(){
    if (this.rmCodeForm.valid) {
      let data = {
        rm_code: this.rmCodeForm.get('rmcode').value
      }
      this.signUpService.submitRMCode(data).subscribe((res) => {
        this.rmcode_msg = "";
        this.router.navigate(['/dashboard'], { replaceUrl: true });
      },
        (err) => {
          this.rmcode_msg = err.error.message;
          console.log(err);
        }
      );
    }
  }

  skipRMCode(){
    this.router.navigate(['/dashboard'], { replaceUrl: true });
  }



}
