import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router,ActivatedRoute  } from '@angular/router';
import { StocksService } from 'src/app/@core/service/stocks/stocks.service';

@Component({
  selector: 'app-transaction-history',
  templateUrl: './transaction-history.component.html',
})
export class TransactionHistoryComponent implements OnInit {
  companyId:string ="";
  transactionHistory:any;
  perPage= 5;
  page= 1;
  loading= true;
  showLoadBtn = true;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private stockService: StocksService,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.companyId = params['id'];
      // console.log("this.route.params.subscribe", this.companyId);
      let data = {
        "perPage": this.perPage,
        "page": this.page,
        "company_id": this.companyId
      }
      this.stockService.getTransactionHistoybyCompany(data).subscribe(res => {
        this.transactionHistory = res.result.records;
        this.handleLoadBtn(res.result.totalCount);
        this.loading= false;

      })
    });
  }

  handleLoadBtn(totalCount){
    if(this.transactionHistory.length < totalCount){
      this.showLoadBtn=true;
    }else{
      this.showLoadBtn=false;

    }
  }


  loadMore(){
    this.loading=true;
    this.page =this.page+1
  this.stockService.getTransactionHistoybyCompany({perPage:this.perPage, page:this.page,company_id: this.companyId}).subscribe((res)=>{
    this.transactionHistory = [...this.transactionHistory, ...res.result.records];
    this.handleLoadBtn(res.result.totalCount);
    this.loading=false;

  })
}

}
