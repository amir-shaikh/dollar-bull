import { Component, OnInit } from '@angular/core';
import { StocksService } from '../../../@core/service/stocks/stocks.service'
@Component({
  selector: 'app-top-gainers',
  templateUrl: './top-gainers.component.html',
})
export class TopGainersComponent implements OnInit {
  constructor(private stockService: StocksService) { }
  tGainers = null
  ngOnInit() {
    this.stockService.getTopGainerLoserStocks({
      limit: 20, offset: 1
    }).subscribe( res => {
      this.tGainers = res
    })
  }
}
