import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllEtfsComponent } from './all-etfs.component';

describe('AllEtfsComponent', () => {
  let component: AllEtfsComponent;
  let fixture: ComponentFixture<AllEtfsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllEtfsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllEtfsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
