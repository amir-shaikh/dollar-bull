import { Component, OnInit } from '@angular/core';
import {StocksService} from '../../../@core/service/stocks/stocks.service'
@Component({
  selector: 'app-most-traded',
  templateUrl: './most-traded.component.html',
  styleUrls: ['./most-traded.component.scss']
})
export class MostTradedComponent implements OnInit {

  constructor(private stocksService: StocksService) { }
  mostTradedStocks = []
  ngOnInit(): void {
    this.stocksService.getMostTradedStocks({
      limit: 3, offset: 1
    })
    .subscribe(res => {
      console.log(res)
      this.mostTradedStocks = res
    })
    
  }

}
