import { TabsI } from './../../../../@shared/component/nav-tabs/nav-tabs.component';
import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbAccordionConfig } from '@ng-bootstrap/ng-bootstrap';
import { NavigationService } from 'src/app/@core/service/navigation.service';
import { CommonService } from '../../../../@core/service/common/common.service'
import { StocksService } from '../../../../@core/service/stocks/stocks.service'

@Component({
  selector: 'app-by-category-layout',
  templateUrl: './by-category-layout.component.html',
  providers: [NgbAccordionConfig]
})
export class ByCategoryLayoutComponent implements OnInit {

  @Output()
  onBackClick: EventEmitter<any> = new EventEmitter<any>();
  @Input()
  disableDefaultBack: boolean = false;
  @Output()
  onSearchChange: EventEmitter<string> = new EventEmitter<string>();

  searchInput: string = '';
  strings: any = {
    searchPlaceholder: 'Search Stocks',
  };
  searchBlock = false;

  filter = []
  isMobileFilterVisible: boolean = false;

  categoryTabs: TabsI[] = [
    {
      link: '/explore/stocks/all-stocks',
      label: 'All Stocks',
    },
    {
      link: '/explore/stocks/all-etfs',
      label: 'All ETFs',
    },
    {
      link: '/explore/stocks/top-gainers',
      label: 'Top Gainers',
    },
    {
      link: '/explore/stocks/top-losers',
      label: 'Top Losers',
    },
    {
      link: '/explore/stocks/most-traded',
      label: 'Most Traded',
    },
  ];

  selectedFilterIndex = 0

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    config: NgbAccordionConfig,
    private navigation: NavigationService,
    private commonService: CommonService,
    private stocksService: StocksService
  ) {
    config.closeOthers = true;
    config.type = 'light';
  }

  searchClickMobile = false

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data) => {
      console.log({ data });
    });

    this.activatedRoute.url.subscribe(() => {
      console.log(this.activatedRoute.snapshot.firstChild?.data);
    });

    //GET FILTER MASTER
    this.commonService.getFilterMaster()
      .subscribe(res => {
        this.filter = res
      })
  }
  handleBackClick(e: any) {
    if (this.onBackClick) {
      this.onBackClick.emit(e);
    }

    if (this.disableDefaultBack) {
      return;
    }
    this.navigation.back();
  }
  handleNavigateSearch(e?: any) {
    this.router.navigate(['explore/stocks/1'])
  }
  handleSearchChange(e: any) {
    this.searchInput = e.target.value;
    console.log(this.searchInput);
    this.searchBlock = true;
    this.onSearchChange.emit(this.searchInput);
    // setTimeout(() => {
    //   this.searchBlock = false;
    // }, 2000);
  }

  handlesearchNavigateMobile() {
    this.searchClickMobile = true
  }

  handleCheckSelectedValue(value: any, selectedFilterIndex: any) {
    let selectedValue = this.filter[selectedFilterIndex].selectedValue

    if (!selectedValue.length) {
      return null
    }

    for (let v of selectedValue) {
      if (value == v) {
        return "checked"
      }
    }

    return null
  }

  handleFilterSelect(filterIndex, filterValue) {
    let indexedFilter = this.filter[filterIndex]
    let selectedFilter = this.commonService.updateSelectedFilter(indexedFilter['name'], filterValue)
  }

  handleApplyFilter() {
    let selectedFilters = this.commonService.selectedFilter$
    //GET ALL STOCKS
    this.stocksService.getAllStocks({
      "offset": 0,
      "limit": 10,
      ...selectedFilters
    }, true)
  }

  handleMobileFilterClick = (e:any) => {
    this.isMobileFilterVisible = true;
  }

  handleMobileFilterClose = (e:any) => {
    this.isMobileFilterVisible = false;
  }

  handleMobileFilterApply = (e:any) => {
    this.isMobileFilterVisible = false;
  }
}
