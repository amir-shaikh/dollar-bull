import { TransactionHistoryComponent } from './transaction-history/transaction-history.component';
import { MostTradedComponent } from './most-traded/most-traded.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StocksCategoryListComponent } from './stocks-category-list/stocks-category-list.component';
import { StocksByIdComponent } from './stocks-by-id/stocks-by-id.component';
import { AllStocksComponent } from './all-stocks/all-stocks.component';
import { AllEtfsComponent } from './all-etfs/all-etfs.component';
import { TopGainersComponent } from './top-gainers/top-gainers.component';
import { TopLosersComponent } from './top-losers/top-losers.component';
import { SearchComponent } from './search/search.component';

import { ByCategoryLayoutComponent } from './components/by-category-layout/by-category-layout.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: StocksCategoryListComponent },
  {
    path: 'search',
    component: SearchComponent,
  },
  {
    path: '',
    component: ByCategoryLayoutComponent,
    children: [
      {
        path: 'all-stocks',
        component: AllStocksComponent,
        data: { title: 'All Stocks', type: 'all-stocks' },
      },
      {
        path: 'all-etfs',
        component: AllEtfsComponent,
        data: { title: 'All Etfs', type: 'all-etfs' },
      },
      {
        path: 'top-gainers',
        component: TopGainersComponent,
        data: { title: 'Top Gainers', type: 'top-gainers' },
      },
      {
        path: 'top-losers',
        component: TopLosersComponent,
        data: { title: 'Top Losers', type: 'top-losers' },
      },
      {
        path: 'most-traded',
        component: MostTradedComponent,
        data: { title: 'Most Traded', type: 'most-traded' },
      },
     
    ],
  },
  { path: ':id', component: StocksByIdComponent },
  { path: ':id/transation-history', component: TransactionHistoryComponent },

  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StocksRoutingModule {}
