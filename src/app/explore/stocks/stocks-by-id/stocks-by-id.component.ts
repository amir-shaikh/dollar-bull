import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router,ActivatedRoute  } from '@angular/router';
import { NgbModalConfig, NgbModal, NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { StocksService } from 'src/app/@core/service/stocks/stocks.service';
import { WatchlistService } from 'src/app/@core/service/watchlist/watchlist.service';
import { CommonService } from '../../../@core/service/common/common.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Modals } from '@capacitor/core';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-stocks-by-id',
  templateUrl: './stocks-by-id.component.html',
  providers: [NgbModalConfig, NgbModal, NgbDropdownConfig],
})


export class StocksByIdComponent implements OnInit {
  @ViewChild('SuccessModal') templateRef: TemplateRef<any>;
  showShortDesciption = true;
  watchListAdded:boolean=false;
  closeResult = '';
  watchlistactive = true
  financialTabs: any[] = [
    {
      link: 'valuation',
      label: 'Valuation',
      preventDefault: true,
    },
    {
      link: 'dividends',
      preventDefault: true,
      label: 'Dividends',
    },
    {
      link: 'balanceSheet',
      preventDefault: true,
      label: 'Balance Sheet',
    },
    {
      link: 'operating_metrics',
      label: 'Operating Metrics',
      preventDefault: true,
    },
    {
      link: 'income_statement',
      label: 'Income Stmt',
      preventDefault: true,
    }
  ];
  activeFinancialTab: string = 'valuation';

  label = [];
  data = [];
  dataSets = [
    {
      data: [],
      label: 'Performance',
    },
  ];

  dataLoaded = false;
  stockPreOrderDetail;
  monthsInPerformanceCharts: any = [
    { text: '1M', value: '1M', selected: true },
    { text: '3M', value: '3M' },
    { text: '6M', value: '6M' },
    { text: '1Y', value: '1Y' },
  ];
  watchlists:any[] =[];
  watchlistLoading:boolean =true;
  companyId:string ="";
  modalReference: any;

  orderForm: FormGroup;
  tradeActionOption={
    BUY:"BUY",
    SELL:"SELL",
  };
  tradeAction:string;
  companyNews:any =[];
  advanceOptionText:boolean=true;
  advanceOptionField:boolean=false;
  stopPriceSection:boolean=false;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    config: NgbModalConfig,
    dropdownConfig:NgbDropdownConfig,
    private watchlistService: WatchlistService,
    private modalService: NgbModal,
    private stockService: StocksService,
    private fb: FormBuilder,
    private commonService: CommonService,
    private toastr: ToastrService
    ) {
    config.backdrop = 'static';
    config.keyboard = false;
    dropdownConfig.placement = 'top-left';
    dropdownConfig.autoClose = false;
  }
  stockStats:any;
  termsDefinationData =[
    {heading : 'Limit Order' , text: 'A limit order is an order to buy or sell a stock at a specific price or better. A buy limit order can only be executed at the limit price or lower, and a sell limit order can only be executed at the limit price or higher'},
    {heading : 'Market Order' , text: 'A market order is an order to buy or sell a security immediately. This type of order guarantees that the order will be executed, but does not guarantee the execution price. A market order generally will execute at or near the current bid (for a sell order) or ask (for a buy order) price.'},
    {heading : 'GTC' , text: 'Good till canceled'},
    {heading : 'FOK' , text: 'Fill or Kill; orders are filled entirely or canceled, partial fills are not accepted'},
    {heading : 'DAY' , text: 'Day, valid until the end of the regular trading session.'},
  ];

  ngOnInit(): void {
    this.orderForm = this.fb.group({
      quantity: [0, Validators.required],
      price: [0, Validators.required,],
      stopPrice: [0],
      orderType: ["Market", Validators.required],
      duration: ["GTC"],

    });
    
    this.orderForm.controls['price'].disable();
    this.orderForm.controls['stopPrice'].disable();

    this.orderForm.get('orderType').valueChanges.subscribe(val => {
        if (val == "Market" || val == "StopMarket") {
          this.orderForm.controls['price'].disable();
        }else{
          this.orderForm.controls['price'].enable();
        }

        if (val == "StopLimit" || val == "StopMarket") {
          this.orderForm.controls['stopPrice'].enable();
        }else{
          this.orderForm.controls['stopPrice'].disable();
        }
        if(val == "StopLimit" || val == "StopMarket"){
          this.stopPriceSection = true;
          this.orderForm.controls['stopPrice'].enable();
        }

        else{
          this.stopPriceSection = false;
          this.orderForm.controls['stopPrice'].disable();
        }
    });
    this.route.params.subscribe(params => {
      this.companyId = params['id'];
      // console.log("this.route.params.subscribe", this.companyId);
      this.stockService.getStockStatsById(this.companyId, true).subscribe(res => {
        this.stockStats = res;
        console.log(["this.stockStats",this.stockStats]);
        
      })
      this.stockService.getCompanyNewsById(this.companyId).subscribe(res=>{
        this.companyNews = res;
        console.log(["this.companyNews",this.companyNews]);
      })
      this.generateGraph(this.companyId, "1M");
    });
  }

 generateGraph(company_id, count){
  this.commonService.getStockPriceChart({
    "company_id": company_id,
    "count": count
  }).subscribe(res => {
    this.label = res['label']
    this.dataSets[0]['data'] = res['data']
    this.dataLoaded = true
  });
 }

 alterDescriptionText() {
    this.showShortDesciption = !this.showShortDesciption
 }
  openBuyStocksModal(BuyStocksModal: any, tradeAction ) {
    this.advanceOptionField=false;
    this.stockPreOrderDetail=null;
    this.stockService.preOrderDetail(this.companyId).subscribe(res => {
      this.stockPreOrderDetail = res;
    })

    this.tradeAction = tradeAction;
    this.modalReference = this.modalService.open(BuyStocksModal);
  }
  openTermsModal(termsModal) {
    this.modalService.open(termsModal);
  }

  openModal(ModelId: any){
    this.modalReference = this.modalService.open(ModelId);
  }

  handleClickViewNewsAll() {
    this.router.navigate(['account/news-articles']);
  }

  changeGraph(month: any) {
    // console.log([month]);
    this.generateGraph(this.companyId, month);
  }

  handleOnClick(e: any) {
    console.log('tabs',e.item.link)
    this.activeFinancialTab = e.item.link;
  }

  handleWatchlistClick(selectWatchList:any) {
    this.watchListAdded = !this.watchListAdded;
    this.getAllWatchlist();
    this.modalService.open(selectWatchList);
  }

  getAllWatchlist(){
    this.watchlistLoading = true;
    this.watchlistService.getWatchlistByCompany(this.companyId).subscribe((res) => {
      this.watchlists = res.map((d:any) => ({
        label: d.watchlistName,
        link: d.watchlistId,
        isAdded: d.isAdded,
        preventDefault: true,
      }));
    this.watchlistLoading = false;
    },
    error=>{
      console.error(error);
      this.watchlistLoading = false;
    }
    )
  }

  handleAddRemoveToWatchlist(watchlistId:string, isAdded :boolean){
    console.log(watchlistId,this.companyId );
    if (isAdded) {
      this.watchlistService.removeCompanyFromWatchlist(watchlistId,this.companyId).subscribe(
        success =>{
         this.watchListAdded  = false;

          console.log(success);
          this.getAllWatchlist();
          this.watchlistService.getCompanyByWatchlist(watchlistId,true)
        },
        failed =>{
          console.error(failed);
          
        }
      )  
    } else {
      this.watchlistService.addCompanyToWatchlist(watchlistId,this.companyId).subscribe(
        success =>{
      this.watchListAdded  = true;
          console.log(success);
          this.getAllWatchlist();
          this.watchlistService.getCompanyByWatchlist(watchlistId,true)
        },
        failed =>{
          console.error(failed);
          
        }
      )      
    }

    
  }

  addStocksIntoWatchListCategory(){
    
  }

  onOrderFormSubmit() {
    if (this.orderForm.valid) {
      let orderData = this.orderForm.value;
      orderData.companyId = this.companyId;
      orderData.tradeAction = this.tradeAction;
      this.stockService.orderStock(orderData).subscribe(
        (success) => {
          this.orderForm.reset();
          this.modalReference.close()
          this.openModal(this.templateRef)
        },
        (error) => {
        // this.toastr.error('Hello world!', 'Toastr fun!');
        console.log(error);
     
          
          console.error(error);
          console.log(error);
          if (error && error.error && error.error.statusCode == 1001) {
            
            this.toastr.error("Please Login with Trade Station");

            //TODO Trade LOGIN Handle
          }else{
            let message =  error.error ? error.error.message :"";
        
            this.toastr.error("", message);
          }
        }
      )

    } else {
      alert('order data is Invalid.');
    }
  }

  handleNavigateOrderStatus(){
    this.modalReference.close()
    this.router.navigate(['account/order'])
  }

  comingSoon(){
    alert("coming soon");
  }

  navigateTransationHistory(){
    if(!this.companyId){ return; }

    this.router.navigate(['explore/stocks/'+this.companyId+'/transation-history'])
  }

  advanceOption(){
    // this.advanceOptionText = !this.advanceOptionText;
    this.advanceOptionField = !this.advanceOptionField;
  }
}
