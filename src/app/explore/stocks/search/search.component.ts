import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { StocksService } from '../../../@core/service/stocks/stocks.service'

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  @Output()
  onSearchChange: EventEmitter<any> = new EventEmitter<any>();
  searchBlock = false;
  searchInput: string = '';
  strings: any = {
    searchPlaceholder: 'Search Stocks',
  };
  searchCategoryTabs : any[]=[
    {
      link: "/explore/stocks",
      label: "All"
    },
    {
      link: "/explore/stocks",
      label: "Stocks"
    },
    {
      link: "/explore/stocks",
      label: "Etf"
    },
    {
      link: "/explore/stocks",
      label: "Indices"
    },
  ]
  constructor(private stockService: StocksService) { }

  searchedResults = []

  ngOnInit(): void {
    
  }

  handleSearch () {
    this.stockService.requestStockSearch(this.searchInput)
    .subscribe(res => {
      this.searchedResults = res.result
    })
    
  }

  handleSearchChange(e: any) {
    this.searchInput = e.target.value;
    this.searchBlock = true;
    this.onSearchChange.emit(this.searchInput);
    this.stockService.requestStockSearch(this.searchInput)
    .subscribe(res => {
      this.searchedResults = res.result
    })
  }
}
