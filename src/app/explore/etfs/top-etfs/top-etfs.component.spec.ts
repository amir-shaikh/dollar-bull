import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopEtfsComponent } from './top-etfs.component';

describe('TopEtfsComponent', () => {
  let component: TopEtfsComponent;
  let fixture: ComponentFixture<TopEtfsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopEtfsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopEtfsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
