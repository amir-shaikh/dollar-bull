import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EtfsRoutingModule } from './etfs-routing.module';
import { TopEtfsComponent } from './top-etfs/top-etfs.component';
import { SharedModule } from 'src/app/@shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CategoryListComponent } from './category-list/category-list.component';


@NgModule({
  declarations: [TopEtfsComponent, CategoryListComponent],
  imports: [
    CommonModule,
    EtfsRoutingModule,
    SharedModule, 
    NgbModule,
  ]
})
export class EtfsModule { }
