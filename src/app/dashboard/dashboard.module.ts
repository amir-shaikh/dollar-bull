import { AccountModule } from './../account/account.module';
import { AccountCardComponent } from './../account/components/accountcard/account-card/account-card.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SharedModule } from '../@shared/shared.module';
import { InvestmentModule } from '../investment/investment.module';



import { WatchlistComponent } from './components/watchlist/watchlist.component';
import { TopGainersComponent } from './components/top-gainers/top-gainers.component';
import { TopEtfProvidersComponent } from './components/top-etf-providers/top-etf-providers.component';
import { TopIndicesComponent } from './components/top-indices/top-indices.component';
import { TopGlobalNewsComponent } from './components/top-global-news/top-global-news.component';
import { CategorywatchlistComponent } from './categorywatchlist/categorywatchlist.component';
import { FundTransferComponent } from './fund-transfer/fund-transfer.component';
import { NetbankingStepComponent } from './fund-transfer/netbanking-step/netbanking-step.component';
import { FundTransferBankComponent } from './fund-transfer/fund-transfer-bank/fund-transfer-bank.component';
import { NavigationScrollComponent } from './components/navigation-scroll/navigation-scroll.component';
import { TopSectorsComponent } from './components/top-sectors/top-sectors.component';
import { ClipboardModule } from 'ngx-clipboard';




@NgModule({
  declarations: [DashboardComponent, WatchlistComponent, TopGainersComponent, TopEtfProvidersComponent, TopIndicesComponent, TopGlobalNewsComponent, CategorywatchlistComponent, FundTransferComponent, NetbankingStepComponent, FundTransferBankComponent, NavigationScrollComponent, TopSectorsComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
    InvestmentModule,
    FormsModule, 
    ReactiveFormsModule,
    AccountModule,
    ClipboardModule
    
  ],
  exports: [AccountCardComponent,TopGainersComponent ,TopEtfProvidersComponent, TopIndicesComponent, TopGlobalNewsComponent, CategorywatchlistComponent,FundTransferComponent]
})
export class DashboardModule { }
