import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopSectorsComponent } from './top-sectors.component';

describe('TopSectorsComponent', () => {
  let component: TopSectorsComponent;
  let fixture: ComponentFixture<TopSectorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopSectorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopSectorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
