import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationScrollComponent } from './navigation-scroll.component';

describe('NavigationScrollComponent', () => {
  let component: NavigationScrollComponent;
  let fixture: ComponentFixture<NavigationScrollComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavigationScrollComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationScrollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
