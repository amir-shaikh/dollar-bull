import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NewsArticlesService } from 'src/app/@core/service/news-articles/news-articles.service';

@Component({
  selector: 'app-top-global-news',
  templateUrl: './top-global-news.component.html',
  styleUrls: ['./top-global-news.component.scss'],
})
export class TopGlobalNewsComponent implements OnInit {
  newsList: any[] = [];
  newSummarylength: number = 150;
  constructor(
    private router: Router,
    private newsArticlesService: NewsArticlesService
  ) {}

  ngOnInit(): void {
    this.newsArticlesService
      .getNewsArticles({
        limit: 5,
        offset: 1,
        company_id: ""
      })
      .subscribe((res) => {
        this.newsList = res;
      });
  }

  handleNewsClick(item:any) {
    window.open(item.url);
    // this.router.navigate([`account/news-articles/${item.company_news_id}`]);
  }
}
