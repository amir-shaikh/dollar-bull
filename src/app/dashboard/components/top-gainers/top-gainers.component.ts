import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavigationService } from 'src/app/@core/service/navigation.service';
import { StocksService } from 'src/app/@core/service/stocks/stocks.service';

@Component({
  selector: 'app-top-gainers',
  templateUrl: './top-gainers.component.html',
  styleUrls: ['./top-gainers.component.scss']
})
export class TopGainersComponent implements OnInit {

  constructor(private stockService: StocksService, private navigationService: NavigationService) { }
  topGainerLoser:any = {};
  limit: number = 20;
  offset: number = 0;

  ngOnInit(): void {
    this.stockService.getTopGainerLoserStocks({
      limit: this.limit, offset: this.offset
    }).subscribe( res => {
      const tG = JSON.parse(JSON.stringify(res))
      // if(this.topGainerLoser && this.topGainerLoser.topgainers && this.topGainerLoser.topgainers.length > 4){
      this.topGainerLoser.topgainers = tG['topgainers'].slice(0,4);
      // }
    })
  }

  handleViewAll(){
    this.navigationService.navigate(['explore/stocks/top-gainers'])
  }
}
