import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/@core/service/common/common.service';
import { WatchlistService } from 'src/app/@core/service/watchlist/watchlist.service';
import { StocksService } from 'src/app/@core/service/stocks/stocks.service';
import { AuthService } from 'src/app/@core/service/auth/auth.service';
import { NavigationService } from 'src/app/@core/service/navigation.service';
import { LogicalProjectPath } from '@angular/compiler-cli/src/ngtsc/file_system';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {
  hasInvested:boolean=false;

  myWatchListData = false;
  myWatchListEmpty = true;
  watchListTabs: any[] = [];
  customer_invested_value = 0;
  customer_current_value = 0;
  profit_loss_value = 0;
  profit_loss_per = "";
  myInvestementData = [];
  portfolioTabs: any[] = [
    {
      label: 'Line Chart',
      link: '0',
      preventDefault: true,
      icon:'assets/icons/line-chart-tab-icon.svg'
    },
    {
      label: 'Pie Chart',
      link: '1',
      preventDefault: true,
      icon:'assets/icons/pie-chart-tab-icon.svg'
    },
  ];
  activePortfolioTab: string = '0';

  label = ['Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  pieLabels = {'etf':"ETFs", "stocks":"Stocks"};

  dataSets = [
    {
      data: [],
      label: 'Performance',
    },
  ];

  monthsInPerformanceCharts: any = [
    { text: '1M', value: '1M', selected: true },
    { text: '3M', value: '3M' },
    { text: '6M', value: '6M' },
    { text: '1Y', value: '1Y' },
  ];

  pieChartLabels = ['Stocks', 'ETFs', 'Bonds'];
  pieChartValues = [30, 50, 20];

  dollarbullStats: any = {};
  performanceData = [];
  dataLoaded = false;
  watchlistCompany = [];
  constructor(private router: Router, private watchlistService: WatchlistService, private commonService: CommonService,private stocksService: StocksService, private authService: AuthService, public navigationService: NavigationService) { }

  ngOnInit(): void {
    this.commonService.getDollarbullStats().subscribe((res) => {
      this.dollarbullStats = res;
    });
    this.getWatchlist();
    this.get_my_investment();
  }

  get_my_investment(){
    var data = {
      perPage :5,
      page :1,
    }
   
    this.stocksService.getOrderList(data).subscribe((res) => {
      this.myInvestementData = res.records;
      if (this.myInvestementData.length > 0) {
        this.authService.saveHasInvested(true);
        this.hasInvested = true;
        this.afterInvestmentData();
      }
      else{
        this.authService.saveHasInvested(false);
      }
      
    });
  }

  getWatchlist(){
    this.watchlistService.getWatchlist().subscribe((res) => {
      this.watchListTabs = res.map((d: any) => ({
        label: d.watchlistName,
        link: d.watchlistId,
        preventDefault: true,
      }))
      if (this.watchListTabs.length>0) {
        this.getWatchlistItem(this.watchListTabs[0]['link']);
      }
      
    });
  }

  afterInvestmentData(){
    this.commonService.getOverview().subscribe((res) => {
      this.customer_invested_value = res.customer_current_value;
      this.customer_current_value = res.customer_invested_value;
      this.profit_loss_value = res.profit_loss_value;
      var pl_per = res.profit_loss_per;
      this.profit_loss_per = pl_per + "%";
    });
    this.commonService.getPerformance(
      {
        customer_id: 1,
        type: "1M"
      }, true
    ).subscribe(res => {
      this.performanceData = res
      this.label = this.performanceData['label']
      this.dataSets[0]['data'] = this.performanceData['data']
      this.dataLoaded = true
    })
    this.commonService.getCustomerInvestmentChart(
      {
        customer_id : 1
      }, true
    ).subscribe(res => {
      console.log(res.labels);
      res.labels.forEach((value, index) => {
        res.labels[index] = this.pieLabels[value]; 
      });
      this.pieChartLabels = res.labels;
      this.pieChartValues = res.data;
      console.log({chart :res})
    })
  }

  navigateToWatchlist() {
    this.router.navigate(['dashboard/categorywatchlist']);
  }

  addStockWatchList() {
    this.myWatchListData = true;
    this.myWatchListEmpty = false;
    setTimeout(() => {
      this.myWatchListData = false;
      this.myWatchListEmpty = true;
    }, 2300);
  }

  handleViewAll() {
    this.router.navigate(['investment']);
  }

  handleMonthPerformance(month: any) {
    this.commonService.getPerformance(
      {
        customer_id: 1,
        type: month
      }, true
    ).subscribe(res => {
      this.performanceData = res
      this.label = this.performanceData['label']
      this.dataSets[0]['data'] = this.performanceData['data']
      this.dataLoaded = true

    })
  }

  handleWatchlistClick(e: any) {
    console.log(["handleWatchlistClick", e]);
    this.getWatchlistItem(e.item.link);
  }

  getWatchlistItem(watchlist_id: any){
    this.watchlistService.getCompanyByWatchlist(watchlist_id).subscribe((success) => {
      console.log(["this.watchlistCompany",success.records]);
      this.watchlistCompany = success.records;
    },fail=>{
      this.watchlistCompany =[];
    });
  }

  handleCompleteProfile(e:any) {
    alert("Coming Soon !!!");
  }

  handleInvestNow(e:any) {
    e.preventDefault();
    e.stopPropagation();
    this.navigationService.navigate(['/explore/stocks']);
  }

  handleOnClick(e: any) {
    console.log('tabs',e.item.link)
    this.activePortfolioTab = e.item.link;
  }
}
