import { FundTransferBankComponent } from './fund-transfer/fund-transfer-bank/fund-transfer-bank.component';
import { NetbankingStepComponent } from './fund-transfer/netbanking-step/netbanking-step.component';
import { FundTransferComponent } from './fund-transfer/fund-transfer.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategorywatchlistComponent } from './categorywatchlist/categorywatchlist.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [{ path: '', component: DashboardComponent },
{ path: 'categorywatchlist', component: CategorywatchlistComponent },
{ path: 'fundtransfer', component: FundTransferComponent },
{ path: 'fundtransfer/netbanckingstep', component: NetbankingStepComponent },
{ path: 'fundtransfer/fundtransferbank', component: FundTransferBankComponent },




];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
