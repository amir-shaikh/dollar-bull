import { Component, OnInit } from '@angular/core';
import { LrsService } from 'src/app/@core/service/lrs/lrs.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-netbanking-step',
  templateUrl: './netbanking-step.component.html',
  styleUrls: ['./netbanking-step.component.scss']
})
export class NetbankingStepComponent implements OnInit {
  bankList:any=[];
  onlineSteps:any=[];
  toastId=0;
  selectedBank:any;
  constructor(private lrsService: LrsService,
    private toastr: ToastrService,
    ) { }

  ngOnInit(): void {
    this.getBanksList();
  }

  getBanksList() {
      this.lrsService.getBanks().subscribe((res) => {
        this.bankList = res;
        this.selectedBank =this.bankList[0];
        this.getBankSteps(this.selectedBank.id);
      });
  }

  

  bankListChange() {
    console.log(this.selectedBank);
    this.getBankSteps(this.selectedBank.id);
  }

  copiedToClipboard($event){
    console.log($event);
    if (this.toastId) {
      this.toastr.remove(this.toastId);
    }
    let {toastId} = this.toastr.info("Copied to clipboard!",$event.content,{
      timeOut: 3000,
      disableTimeOut:false,

      // toastId:this.toastId,
      newestOnTop:true,
      
    });
    this.toastId = toastId;
    
  }
  getBankSteps(bank_id) {
    let body = {
      "bank_master_id": bank_id
    }
    this.lrsService.RemitOnline(body).subscribe((res) => {
      this.onlineSteps = res;
    });
  }

}
