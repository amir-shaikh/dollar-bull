import { Component, OnInit } from '@angular/core';
import { LrsService } from 'src/app/@core/service/lrs/lrs.service';

@Component({
  selector: 'app-fund-transfer-bank',
  templateUrl: './fund-transfer-bank.component.html',
  styleUrls: ['./fund-transfer-bank.component.scss']
})
export class FundTransferBankComponent implements OnInit {

  constructor(private lrsService: LrsService,) { }
  bankList:any=[];
  pdfPath:string="";
  ngOnInit(): void {
    this.getBanksList();
  }

  getBanksList() {
      this.lrsService.getBanks().subscribe((res) => {
        this.bankList = res;
        this.getBankSteps(this.bankList[0].id);
      });
  }

  bankListChange(target: EventTarget) {
    let bank_id = (target as HTMLInputElement).value;
    this.getBankSteps(bank_id);
  }


  getBankSteps(bank_id) {
    let body = {
      "bank_master_id": bank_id
    }
    this.lrsService.RemitOffline(body).subscribe((res) => {
      this.pdfPath = res;
      console.log(this.pdfPath);
    });
  }

  downloadPDF() {
      window.open(this.pdfPath); 
      var a         = document.createElement('a');
      a.href        = this.pdfPath; 
      a.target      = '_blank';
      a.download    = 'A2Form.pdf';
      document.body.appendChild(a);
      a.click();
  }

}