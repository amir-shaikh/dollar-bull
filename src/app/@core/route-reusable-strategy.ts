import {
  ActivatedRouteSnapshot,
  DetachedRouteHandle,
  RouteReuseStrategy,
} from '@angular/router';
import { Injectable } from '@angular/core';

/**
 * A route strategy allowing for explicit route reuse.
 * Used as a workaround for https://github.com/angular/angular/issues/18374
 * To reuse a given route, add `data: { reuse: true }` to the route definition.
 *
 * More detail information
 * https://medium.com/javascript-in-plain-english/angular-route-reuse-strategy-b5d40adce841
 */
@Injectable()
export class RouteReusableStrategy extends RouteReuseStrategy {
  // Determines if this route (and its subtree) should be detached to be reused later.
  public shouldDetach(route: ActivatedRouteSnapshot): boolean {
    return false;
  }

  // Stores the detached route.
  public store(
    route: ActivatedRouteSnapshot,
    detachedTree: DetachedRouteHandle | null
  ): void {}

  // Determines if this route (and its subtree) should be reattached.
  public shouldAttach(route: ActivatedRouteSnapshot): boolean {
    return false;
  }

  // Retrieves the previously stored route.
  public retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle | null {
    return null;
  }

  // Determines if a route should be reused.
  //   Added reuse keyword to update config manually from route
  public shouldReuseRoute(
    future: ActivatedRouteSnapshot,
    curr: ActivatedRouteSnapshot
  ): boolean {
    return future.routeConfig === curr.routeConfig || future.data.reuse;
  }
}
