import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpRequest,
  HttpHandler,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, delay, finalize, retry } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { LoggerService } from '../service/logger.service';
import { NavigationService } from '../service/navigation.service';
import { CommonService } from '../service/common/common.service';

const log = new LoggerService('ResponseInterceptor');

@Injectable({
  providedIn: 'root',
})
export class ResponseInterceptor implements HttpInterceptor {
  constructor(private navigationService: NavigationService, private commonService: CommonService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // allow to retry api call upto 2 times before catchin an error
    return next.handle(req).pipe(
        finalize(() => this.commonService.setLoader(false))
    );
  }
}
