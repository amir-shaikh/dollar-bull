import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpRequest,
  HttpHandler,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { LoggerService } from '../service/logger.service';
import { NavigationService } from '../service/navigation.service';
import { ToastrService } from 'ngx-toastr';

const log = new LoggerService('ErrorInterceptor');

@Injectable({
  providedIn: 'root',
})
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private navigationService: NavigationService, 
    private toastr: ToastrService
    ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // allow to retry api call upto 2 times before catchin an error
    return next.handle(req).pipe(retry(2), catchError(this.errorHandler));
  }

  private errorHandler(error: HttpErrorResponse): Observable<HttpEvent<any>> {

    if (error.status === 401 || error.status === 403) {
      // TODO: Handle logic for non auth and forbidden status
    }

    if (error.status === 404) {
      // TODO: Handle logic for not found api
      alert('not found');
    }
    if (error.status === 400) {
      // console.log({error});
      // let message =  error.error ? error.error.message :"";
      // this.toastr.error(message);
      
    }

    if (environment.logApi) {
      try {
        log.error({error});
        log.error("error", JSON.stringify(error));
      } catch (e) {}
    }

    return throwError(error);
  }
}
