import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpRequest,
  HttpHandler,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { JwtService } from '../service/jwt.service';
import {CommonService} from '../service/common/common.service'

@Injectable({
  providedIn: 'root',
})
export class RequestInterceptor implements HttpInterceptor {
  constructor(private jwtService: JwtService, private commonService: CommonService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // Update content type if not provided
    if (!req.headers.has('Content-Type')) {
      req = req.clone({
        setHeaders: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
      });
    }

    this.commonService.setLoader(true);

    // generate endpoint url if complete url is not provided
    if (!/^(http|https):/i.test(req.url)) {
      let reqUrl = req.url;
      
      if (reqUrl.indexOf("@UserAPI") !== -1) {
        reqUrl = reqUrl.replace("@UserAPI", environment.userAPIUrl);
      }

      if (reqUrl.indexOf("@DataAPI") !== -1) {
        reqUrl = reqUrl.replace("@DataAPI", environment.dataAPIUrl);
      }

      req = req.clone({ url: reqUrl });
    }

    // add authentication for req
    req = this.addAuthenticationToken(req);
    
    return next.handle(req);
  }

  private addAuthenticationToken(req: HttpRequest<any>): HttpRequest<any> {
    const token = this.jwtService.getToken();
    // If token is not present then return request
    if (!token) {
      return req;
    }

    // TODO: (Optional)
    // Additional to add authorization only for specific domain
    

    // else add authorization header
    return req.clone({
      setHeaders: { Authorization: `Bearer ${token}` },
    });
  }
}
