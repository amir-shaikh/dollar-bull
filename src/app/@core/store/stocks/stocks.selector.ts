import { createSelector } from "@ngrx/store";

export const topGainers = createSelector(
  (state: any) => state.stocks.topGainers,
  (topGainers: any) => topGainers
);

export const topEtfs = createSelector(
  (state: any) => state.stocks.topEtfs,
  (topEtfs: any) => topEtfs
);

export const topIndices = createSelector(
  (state: any) => state.stocks.topIndices,
  (topIndices: any) => topIndices
);