import { createReducer, on } from '@ngrx/store';

import { getTopStocksSuccess, getTopEtfsSuccess, getTopIndicesSuccess, getAllStocks } from './stocks.action'

interface stockState {
    topGainers: [];
    topLosers: [];
    mostTraded: [];
    topEtfs: [];
    topIndices: [];
    allStocks: [];
}

export const initialState: stockState = {
    topGainers: [],
    topLosers: [],
    mostTraded: [],
    topEtfs: [],
    topIndices: [],
    allStocks: []
};

export const stocksReducer = createReducer(
    initialState,
    on(getTopStocksSuccess, (state, props: any) => (
        {
            ...state,
            topGainers: props.payload.top_gainers,
            topLosers: props.payload.top_losers,
            mostTraded: props.payload.most_traded
        })),
    on(getTopEtfsSuccess, (state, props: any) => ({ ...state, topEtfs: props.payload })),
    on(getTopIndicesSuccess, (state, props: any) => ({ ...state, topIndices: props.payload })),
    on(getAllStocks, (state, props:any) => ({ ...state, allStocks: props.payload }))
);