import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

import { StocksActionTypes } from './stocks.action'
import { StocksService } from '../../service/stocks/stocks.service'

@Injectable()
export class StockEffects {

    constructor(
        private actions$: Actions,
        private stocksService: StocksService
    ) { }

    handleEffectTopStock() {
        //API CALL
        const response = this.stocksService.getTopStocks()
        if (!response) {
            //FOR CATCH 
        }

        return of({ type: StocksActionTypes.GetTopStocksSuccess, payload: response })
    }

    handleEffectTopEtf() {
        //API Call
        const response = this.stocksService.getTopEtfs({})
        if (!response) {
            //FOR CATCH 
        }

        return of({ type: StocksActionTypes.GetTopEtfsSuccess, payload: response })
    }

    handleEffectTopIndices() {
        //API Call
        const response = {}
        // const response = this.stocksService.getTopIndices()
        if (!response) {
            //FOR CATCH 
        }

        return of({ type: StocksActionTypes.GetTopIndicesSuccess, payload: response })
    }

    handleEffectAllStocks(payload:any) {
        //API Call 
        const response = this.stocksService.getAllStocks(payload)
        if (!response) {
            //FOR CATCH 
        }
        
        return of({ type: StocksActionTypes.GetAllStocksSuccess, payload: response })

    }

    // TOP StockS EFFECT
    loadTopStockEffect$ = createEffect((): any => {
        return this.actions$.pipe(
            ofType(StocksActionTypes.GetTopStocks),
            mergeMap(
                () => this.handleEffectTopStock()
            )
        )
    });


    // TOP Etf EFFECT
    loadTopEtfEffect$ = createEffect((): any => {
        return this.actions$.pipe(
            ofType(StocksActionTypes.GetTopEtfs),
            mergeMap(
                () => this.handleEffectTopEtf()
            )
        )
    });

    // TOP indices EFFECT
    loadTopIndicesEffect$ = createEffect((): any => {
        return this.actions$.pipe(
            ofType(StocksActionTypes.GetTopIndices),
            mergeMap(
                () => this.handleEffectTopIndices()
            )
        )
    });

    //All Stocks Effect
    loadAllStocksEffect$ = createEffect((): any => {
        return this.actions$.pipe(
            ofType(StocksActionTypes.GetAllStocks),
            mergeMap(
                (action) => this.handleEffectAllStocks(action)
            )
        )
    });
}