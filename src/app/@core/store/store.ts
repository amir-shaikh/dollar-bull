
import { stocksReducer } from './stocks/stocks.reducer'
import { filterReducer } from './filter/filter.reducer'
import { dashboardReducer } from './dashboard/dashboard.reducer'

import { StockEffects } from './stocks/stocks.effect'
import { FilterEffects } from './filter/filter.effect'

export const combineReducers = () => {
    return {
        stocks: stocksReducer,
        filter: filterReducer,
        dashboard: dashboardReducer
    }
}

export const combineEffects = () => {
    return [
        StockEffects,
        FilterEffects
    ]
}