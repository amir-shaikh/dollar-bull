import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

import { FilterActionTypes } from './filter.action'
import { CommonService } from '../../service/common/common.service'

@Injectable()
export class FilterEffects {

    constructor(
        private actions$: Actions,
        private commonService: CommonService
    ) { }

    handleEffectFilterMaster() {
        //API Call 
        const response = this.commonService.getMasterFilters()
        if (!response) {
            //FOR CATCH 
        }

        return of({ type: FilterActionTypes.GetMasterFiltersSuccess, payload: response })
    }

    loadFilterMaster$ = createEffect((): any => {
        return this.actions$.pipe(
            ofType(FilterActionTypes.GetMasterFilters),
            mergeMap(
                (action) => this.handleEffectFilterMaster()
            )
        )
    })
}
