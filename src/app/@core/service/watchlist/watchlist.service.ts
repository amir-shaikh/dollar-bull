import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, shareReplay, switchMap } from 'rxjs/operators';
import { Observable, of, timer } from 'rxjs';
import { watchlistParser } from './watchlist.parser';

@Injectable({
  providedIn: 'root',
})
export class WatchlistService {
  private urls = {
    watchlistList: '@UserAPI/watchlist',
    createWatchlist: '@UserAPI/watchlist',
    deleteWatchlist: '@UserAPI/watchlist/{watchlist_id}',
    updateWatchlist: '@UserAPI/watchlist/{watchlist_id}',

    listByWatchlist: '@UserAPI/watchlist/{watchlist_id}/company',
    addToWatchlist: `@UserAPI/watchlist/{watchlist_id}/company`,
    removeFromWatchlist: '@UserAPI/watchlist/{watchlist_id}/company/{company_id}',

    watchlistByCompany: '@UserAPI/watchlist/company/{company_id}',
  };

  //   @ts-ignore
  private watchlistCache: Observable<any>;
  //   @ts-ignore
  private watchlistCompanyCache: {
    [key: string]: Observable<any>
  } = {}

  constructor(private httpClient: HttpClient) { }

  //   Example for auto refresh list
  public getWatchlistTimer(): Observable<any> {
    if (!this.watchlistCache) {
      const timer$ = timer(0, 10000);

      this.watchlistCache = timer$.pipe(
        switchMap((_) => this.requestWatchlist()),
        shareReplay(1)
      );
    }

    return this.watchlistCache;
  }

  //   Example for simple cache
  public getWatchlist(forceCall = false): Observable<any> {
    if (!this.watchlistCache || forceCall) {
      this.watchlistCache = this.requestWatchlist().pipe(shareReplay(1));
    }
    return this.watchlistCache;
  }

  private requestWatchlist() {
    return this.httpClient.get(this.urls.watchlistList).pipe(map(watchlistParser.list));
  }

  // Create New Watchlist
  public createWatchlist(data: any): Observable<any> {
    return this.requestCreateWatchlist(data).pipe(shareReplay(1));
  }

  private requestCreateWatchlist({ watchlistName }: { watchlistName: string }) {
    const data = {
      watchlist_name: watchlistName
    }
    return this.httpClient.post(this.urls.createWatchlist, data).pipe(map(watchlistParser.watchlist));
  }

    // Update Watchlist name
    public updateWatchlist(data: any): Observable<any> {
      return this.requestupdateWatchlist(data).pipe(shareReplay(1));
    }
  
    private requestupdateWatchlist({ watchlistName, watchlistId }: { watchlistName: string, watchlistId:number }) {
      const data = {
        watchlist_name: watchlistName
      }
    let updateUlr = this.urls.updateWatchlist.replace("{watchlist_id}", `${watchlistId}`);

      return this.httpClient.put(updateUlr, data).pipe(map(watchlistParser.watchlist));
    }

  // Delete Watchlist
  public deleteWatchlist(watchlistId: string): Observable<any> {
    return this.requestDeleteWatchlist(watchlistId).pipe(shareReplay(1));
  }

  private requestDeleteWatchlist(watchlistId: string) {
    let deleteUlr = this.urls.deleteWatchlist.replace("{watchlist_id}", `${watchlistId}`);

    return this.httpClient.delete(deleteUlr).pipe(map((res: any) => {
      if (res.status === 1) { delete this.watchlistCompanyCache[watchlistId]; }
      watchlistParser.common(res);
    }));
  }

  // Add Company to Watchlist
  public addCompanyToWatchlist(watchlistId: string, companyId: string): Observable<any> {
    return this.requestaddCompanyToWatchlist(watchlistId, companyId).pipe(shareReplay(1));
  }

  private requestaddCompanyToWatchlist(watchlistId: string, companyId: string) {
    const data = {
      company_id: companyId
    }
    let url = this.urls.addToWatchlist.replace("{watchlist_id}", `${watchlistId}`);
    delete this.watchlistCompanyCache[watchlistId];
    return this.httpClient.post(url, data).pipe(map(watchlistParser.addCompany));
  }

  // Get Company List by Watchlist ID
  public getCompanyByWatchlist(watchlistId: string, forceCall = false): Observable<any> {
    if (!this.watchlistCompanyCache || !this.watchlistCompanyCache[watchlistId] || forceCall) {
      this.watchlistCompanyCache[watchlistId] = this.requestCompanyByWatchlist(watchlistId).pipe(shareReplay(1));
    }
    return this.watchlistCompanyCache[watchlistId];
  }

  private requestCompanyByWatchlist(watchlistId: string) {
    let url = this.urls.listByWatchlist.replace("{watchlist_id}", `${watchlistId}`);
    return this.httpClient.get(url).pipe(map(watchlistParser.companyList));
  }

  // Remove Company from Watchlist
  public removeCompanyFromWatchlist(watchlistId: string, companyId: string): Observable<any> {
    return this.requestRemoveCompanyFromWatchlist(watchlistId,  companyId).pipe(shareReplay(1));
  }

  private requestRemoveCompanyFromWatchlist(watchlistId: string,  companyId: string) {
    let url = this.urls.removeFromWatchlist.replace("{watchlist_id}", `${watchlistId}`);
    url = url.replace("{company_id}", `${companyId}`);
    delete this.watchlistCompanyCache[watchlistId];
    return this.httpClient.delete(url).pipe(map(watchlistParser.common));
  }

  
  //   Example for simple cache
  public getWatchlistByCompany(companyId:string): Observable<any> {
    return this.requestWatchlistByCompany(companyId).pipe(shareReplay(1));
  }

  private requestWatchlistByCompany(companyId:string) {
    let url = this.urls.watchlistByCompany.replace("{company_id}", `${companyId}`);
    return this.httpClient.get(url).pipe(map(watchlistParser.watchlistByCompany));
  }
}
