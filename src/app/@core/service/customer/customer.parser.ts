
const submitQuery = (res: any) => {
    let result: any = {
        message:""
    };

    if (res && res.message) {
        res = res.message;
    }

    if (!res) {
        return result;
    }
    result.message = res
    return result;
}
export const customerParser = {
    submitQuery,
};