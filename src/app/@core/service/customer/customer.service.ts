import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { filter, map, shareReplay, tap } from 'rxjs/operators';
import { customerParser } from './customer.parser'
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CustomerService {
  private urls = {
    submitQuery: '@UserAPI/submitquery',
  };

  constructor(private httpClient: HttpClient) { }


    // Customer Query
    public submitQuery(payload: any): Observable<any> {
      return this.requestsubmitQuery(payload).pipe(shareReplay(1));
    }
  
    private requestsubmitQuery({ query, description }: { query: string , description: string }) {
      const data = {
        query,
        description
      }
      return this.httpClient.post(this.urls.submitQuery, data).pipe(map(customerParser.submitQuery));
    }

}
