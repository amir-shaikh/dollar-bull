import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { Observable } from 'rxjs';
const { Device } = Plugins;

@Injectable({
  providedIn: 'root',
})
export class PlatformService {
  private deviceInfo: any = null;
  private platform: 'web' | 'android' | 'web' = 'web';

  constructor() {
    Device.getInfo()
      .then((info: any) => {
        this.deviceInfo = info;
        this.platform = info.platform;
      })
      .catch((err) => {
        console.log('Device info get error', err);
        this.deviceInfo = null;
        this.platform = 'web';
      });
  }

  //   get platform info for both web and app
  getPlatform() {
    return new Observable<any>((observer) => {
      observer.next(this.platform);
    });
  }

  //   get device info spcifically for apps
  getDeviceInfo() {
    return new Observable<any>((observer) => {
      observer.next(this.deviceInfo);
    });
  }

  //   check current platform is matching
  isPlatform(name: 'web' | 'android' | 'web'):boolean {
    return this.platform === name ? true : false;
  }
}
