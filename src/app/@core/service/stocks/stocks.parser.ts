export const topStocksParser = (response: any) => {
    let topGainers: any = []
    let topLosers: any = []
    let mostTraded: any = []

    let resp: any = {
    }

    if (!response['topgainers']) {
        resp['topgainers'] = topGainers
    } else {
        resp['topgainers'] = commonStocksResponse(response['topgainers'], [])
    }

    if (!response['toplosers']) {
        resp['toplosers'] = topLosers
    } else {
        resp['toplosers'] = commonStocksResponse(response['toplosers'], [])
    }

    if (!response['most_traded']) {
        resp['most_traded'] = mostTraded
    } else {
        resp['most_traded'] = commonStocksResponse(response['most_traded'], [])
    }

    return resp
}

export const topEtfsParser = (response: any) => {
    let list: any ={
        companies:[],
        stocksCount:0
    }
    if (!response || !response.companies || !response.companies.length) {
        return list;
    }

    let stocksCount = response.stocks ? response.stocks: 0
    return {companies : commonStocksResponse(response.companies, []),stocksCount}
}

export const topIndicesParser = (response: any) => {
    let list: any = []

    if (!response) {
        return list;
    }

    if (!response.length) {
        return list;
    }

    return commonStocksResponse(response, list)
}

export const allStocksParser = (response: any) => {
    const stocksCount = response.result.stocks ? response.result.stocks : 0
    response = response.result.companies
    let list: any = [];
    if (!response) {
        return list;
    }

    if (!response.length) {
        return list;
    }

    const companyList = commonStocksResponse(response, list)
    return {companyList, stocksCount}
}

export const stockStatsParser = (response: any) => {
    let resp: any = {};
    if (!response || !response.result) {
        return resp;
    }
    if (response && response.result) {
        response = response.result;
    }

    resp['company_stats'] = {};
    resp['company'] = {};
    resp['market_stats'] = [];
    resp['valuation'] = [];
    resp['dividends'] = [];
    resp['balance_sheet'] = [];
    resp['operating_metrics'] = [];
    resp['income_statement'] = [];
    var tmpArray : any = []

    if (response['company_stats']) {
        resp['company_stats'] = response['company_stats'];
    }
    if (response['company']) {
        resp['company'] = response['company'];
    }
    if (response['market_stats']) {
        resp['market_stats'] = response['market_stats'];
    }
    if (response['valuation']) {
        resp['valuation'] = response['valuation'];
    }
    if (resp['dividends']) {
        resp['dividends'] = response['dividends'];
    }
    if (resp['balance_sheet']) {
        resp['balance_sheet'] = response['balance_sheet'];
    }
    if (response['operating_metrics']) {
        resp['operating_metrics'] = response['operating_metrics'];
    }
    if (response['income_statement']) {
        resp['income_statement'] = response['income_statement'];
    }

    return resp
}

export const mostTradedStocksParser = (response: any) => {

    let list: any = [];
    if (!response) {
        return list;
    }

    if (!response.length) {
        return list;
    }

    return commonStocksResponse(response, list)
}

export const stockDetailsParser = (response: any) => {
    let resp: any = {};
    if (!response) {
        return resp;
    }

    resp['about'] = response['about'] ? response['about'] : "";
    resp['volume'] = response['volume'] ? response['volume'] : "";
    resp['market_capitalisation'] = response['market_capitalisation'] ? response['market_capitalisation'] : "";

    return resp
}

function commonStocksResponse(res: any, list: Array<any>) {
    res.forEach((res: any) => {
        let listItem: any = {}

        listItem['company'] = res['name'] ? res['name'] : "";
        listItem['logo'] = res['logo'] ? res['logo'] : "";
        listItem['price'] = res['current_price'] ? res['current_price'] : "";
        listItem['symbol'] = res['symbol'] ? res['symbol'] : "";
        listItem['gain'] = res['price_diff'] ? res['price_diff'] : "";
        listItem['sector'] = res['sector'] ? res['sector'] : "";
        listItem['market_cap'] = res['market_cap'] ? res['market_cap'] : "";
        listItem['companyId'] = res['company_id'] ? res['company_id'] : "";
        listItem['stock_percent'] = res['stock_percent'] ? res['stock_percent'] : ""
        listItem['yesterday_price'] = res['yesterday_price'] ? res['yesterday_price'] : ""
        list.push(listItem)
    })
    return list
}

export const orderStockResponse = (res: any) => {
    let result: any = "";
    if (res && res.result) {
        res = res.result;
    }

    if (!res || !res.result) {
        return result;
    }
    result = res;
    return result;
};


export const companyNewsResponse = (res: any) => {
    let result: any = "";
    if (!res || !res.result) {
        return result;
    }
    if (res && res.result) {
        result = res.result;
    }
    return result;
}

export const searchStocks = (res: any) => {
    let result = []
    if (res && res.result) {
        res = res.result;
    }

    if (!res || !res.result) {
        return result;
    }
    result = res;
    return result;
     return result;
}

export const OrderListParser = (res: any) => {
    let result: any = {
      page: 0,
      perPage: 0,
      totalCount: 0,
      records: [
      ]
    }
  
    if (res && res.result) {
      res = res.result;
    }
  
    if (!res || !res.records ||!res.records.length) {
      return result;
    }
  
    result.page = res.page ? res.page : 0,
    result.perPage = res.perPage ? res.perPage : 0,
    result.totalCount = res.totalCount ? res.totalCount : 0,
    result.records = res.records.map((r: any) => {
      const item = {
        companyId : r.company_id ? r.company_id : 0,
        name: r["company_name"] ? r["company_name"] : '',
        symbol: r["company_symbol"] ? r["company_symbol"] : '',
        quantity: r.quantity ? r.quantity : 0,
        buyPrice: r.buy_price ? r.buy_price : 0.0,
        price: r["company_current_price"] ? r["company_current_price"] : 0,
      };
      return item;
    });
  
    return result;
  };

export const pendingOrderResponse = (res: any) => {
    let result: any = {
        page: 0,
        perPage: 0,
        totalCount: 0,
        records: [
        ]
      }
    
      if (res && res.result) {
        res = res.result;
      }
    
      if (!res || !res.records ||!res.records.length) {
        return result;
      }
    
      result.page = res.page ? res.page : 0,
      result.perPage = res.perPage ? res.perPage : 0,
      result.totalCount = res.totalCount ? res.totalCount : 0,
      result.records = res.records.map((r: any) => {
        const item = {
        companyId : r.company_id ? r.company_id : 0,
          name: r.company &&  r.company.name ? r.company.name : '',
          symbol: r.company &&  r.company.symbol ? r.company.symbol : '',
          quantity: r.quantity ? r.quantity : 0,
          buyPrice: r.price ? r.price : 0.0,
          price: r.company &&  r.company.current_price ? r.company.current_price : 0.0,
          orderId : r.order_id ? r.order_id : 0,

        };
        return item;
      });
    
      return result;
};

export const preOrderDetailResponse = (res: any) => {
    let result: any = {
        price: 0,
        priceDiff: 0,
        priceDiffPercentage: 0,
        accountBalance: 0,
        duration: [],
        maxBuy: 0,
        orderType: [],
        quantity:0,
        avgPrice:0,
      }
    
      if (res && res.result) {
        res = res.result;
      }
    
      if (!res) {
        return result;
      }

      result = {
        price: res.price ? res.price : 0,
        priceDiff: res.priceDiff ? res.priceDiff : 0,
        priceDiffPercentage: res.priceDiffPercentage ? res.priceDiffPercentage : 0,
        accountBalance: res.accountBalance ? res.accountBalance : 0,
        maxBuy: res.maxBuy ? res.maxBuy : 0,
        orderType: res.orderType ? res.orderType : [],
        duration: res.duration ? res.duration : [],
        quantity: res.quantity ? res.quantity : 0,
        avgPrice: res.avgPrice ? res.avgPrice : 0,
      }
    
      return result;
};
