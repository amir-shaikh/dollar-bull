import { Injectable } from '@angular/core'; 
import { HttpClient } from '@angular/common/http';
import { map, shareReplay, tap } from 'rxjs/operators';

import {
  topStocksParser,
  topEtfsParser,
  topIndicesParser,
  allStocksParser,
  stockDetailsParser,
  stockStatsParser,
  mostTradedStocksParser,
  orderStockResponse,
  companyNewsResponse,
  searchStocks,
  OrderListParser,
  pendingOrderResponse,
  preOrderDetailResponse,
} from './stocks.parser';
import { StocksListPayloadI } from './stocks.interface';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StocksService {

  urls = {
    stocksList: "@DataAPI/stocks",
    stocksDetails: "@DataAPI/stock/details/{company_id}",
    topGainerLoser: "@DataAPI/top_gainers_losers",
    allStocks: "@DataAPI/stocks",
    topEtfs: "@DataAPI/etfs",
    mostTradedStocks: "@DataAPI/most_traded",
    filter: "@DataAPI/filter",
    orderStock: "@UserAPI/createOrder",
    companyNews: "@DataAPI//news_articles",
    searchStocks: "@DataAPI/search_company/{company}",
    orderlist: `@UserAPI/my_investement`,
    pending: `@UserAPI/pendingOrder`,
    investmentsByCompany: `@UserAPI/my_investement/company_id`,
    preOrderDetail: "@UserAPI/preOrderDetail/{company_id}",
    cancelOrder: "@UserAPI/order/{order_id}",
  };

  constructor(
    private httpClient: HttpClient,

  ) { }

  //@ts-ignore
  private topGainerLoserStock$: Observable<any>
  //@ts-ignore
  private topEtfs$: Observable<any>
  //@ts-ignore
  private stockStats$: Observable<any>;
  //@ts-ignore
  private getOrderList$: Observable<any>;
  
  //@ts-ignore
  private allStocksCache$: Observable<any>
  //@ts-ignore
  private mostTradedCache$: Observable<any>
  //@ts-ignore
  private companyNews$: {
    [key: string]: Observable<any>
  } = {}
  //@ts-ignore
  private searchedRes

  topOfStocks = {
    top_gainers: [
      {
        company: 'Amazon.com',
        symbol: 'AMAZON',
        price: '$3,294.62',
        logo: 'assets/icons/amazon-65-675861.webp',
        market_cap: 'Mid Cap',
        sector: 'IT Software',
        gain: '00.31%',
      },
      {
        company: 'Apple',
        symbol: 'APPLE',
        price: '$3,294.62',
        logo: 'assets/icons/amazon-65-675861.webp',
        market_cap: 'Mid Cap',
        sector: 'IT Software',
        gain: '00.31%',
      },
      {
        company: 'Amazon.com',
        symbol: 'Amazon',
        price: '$3,294.62',
        logo: 'assets/icons/amazon-65-675861.webp',
        market_cap: 'Mid Cap',
        sector: 'IT Software',
        gain: '00.31%',
      },
    ],

    top_losers: [],

    most_traded: [],
  };

  topEtfs = [
    {
      company: 'S&P 500 ETF Trust',
      price: '$3427',
      symbol: 'SPY',
      logo: '',
      sector: '',
      market_cap: '',
      gain: '00.21%',
    },
  ];
  
  stockDetails = {
    about:
      ' Apple Inc. designs, manufactures and markets mobile communication and media devices, personal computers and portable digital music players. The Company sells a range of related software, services, accessories, networking solutions, and third-party digital content and applications. The Company’s segments include the Americas, Europe, … ',
    volume: '1.342T',
    market_capitalisation: '2,076.00',
  };

  public getTopStocks() {
    return this.httpClient.get<any>(`${this.urls.topGainerLoser}`).pipe(tap(res => topStocksParser(res)));
  }

  public getStocksById(payload: any) {
    return stockDetailsParser(this.stockDetails);
  }

  // get top gainers and losers
  public getTopGainerLoserStocks(payload: any, forceCall: boolean = false) {
    if (!this.topGainerLoserStock$ || forceCall) {
      this.topGainerLoserStock$ = this.requestTopGainerLoserStocks(payload).pipe(shareReplay(1));
    }
    return this.topGainerLoserStock$;
  }

  private requestTopGainerLoserStocks(payload: any) {
    return this.httpClient.post<any>(`${this.urls.topGainerLoser}`, payload).pipe(map(res => topStocksParser(res.result)))
  }

  public getTransactionHistoybyCompany(payload: any) {
    var url = "@UserAPI/getOrders?page=";
    return this.httpClient.get<any>(url+`${payload.page}&perPage=${payload.perPage}&company_id=${payload.company_id}`).pipe(shareReplay(1));
  }

  // get top etfs
  public getTopEtfs(payload: any, forceCall: boolean = false) {
    if (!this.topEtfs$ || forceCall) {
      this.topEtfs$ = this.requestTopEtfs(payload).pipe(shareReplay(1));
    }
    return this.topEtfs$;
  }

  private requestTopEtfs(payload: any) {
    return this.httpClient.post<any>(`${this.urls.topEtfs}`, payload).pipe(map(res => topEtfsParser(res.result)))
  }

  // get all stocks with filter
  public getAllStocks(payload: any, forceCall: boolean = false): Observable<any> {
    if (!this.allStocksCache$ || forceCall) {
      this.allStocksCache$ = this.requestAllStocks(payload).pipe(shareReplay(1));
    }
    return this.allStocksCache$;
  }

  private requestAllStocks(payload: any) {
    return this.httpClient.post<any>(this.urls.allStocks, payload).pipe(map(res => allStocksParser(res)));
  }

  // get stock by id for details
  public getStockStatsById(companyId: string, forceCall: boolean = false) {
    if (!this.stockStats$ || forceCall) {
      this.stockStats$ = this.requestStockStatsById(companyId).pipe(shareReplay(1));
    }
    return this.stockStats$;
  }

  private requestStockStatsById(companyId: any) {
    let url = this.urls.stocksDetails.replace("{company_id}", `${companyId}`);

    return this.httpClient.get<any>(`${url}`).pipe(map(stockStatsParser))
  }

  public getOrderList(data: any,forceCall: boolean = true){
    if(!this.getOrderList$ || forceCall) {
      this.getOrderList$ = this.requestOrderList(data).pipe(shareReplay(1));
    }
    return this.getOrderList$;
  }
  private requestOrderList(obj:any){
    return this.httpClient.post<any>(`${this.urls.orderlist}`,obj).pipe(map(OrderListParser))
  }
  
  //
  public getMostTradedStocks(payload: any, apiMustHit: boolean = false): Observable<any> {
    if (!this.mostTradedCache$ || apiMustHit) {
      this.mostTradedCache$ = this.requestMostTraded(payload).pipe(shareReplay(1))
    }
    return this.mostTradedCache$
  }

  private requestMostTraded(payload: any) {
    return this.httpClient.post<any>(`${this.urls.mostTradedStocks}`, payload).pipe(map(res => mostTradedStocksParser(res.result.companies)))
  }

  public requestStockSearch(company: string,type=null) {
    let url = this.urls.searchStocks.replace("{company}", `${company}`);
    if (type) {
      url =`${url}/${type}`
    }
    return this.httpClient.get<any>(url)
  }
  public getInvestmentByCompany(payload) {
    return this.httpClient.post<any>(this.urls.investmentsByCompany, payload).pipe(shareReplay(1));
  }

  // Order Stock (BUY or SELL)
  public orderStock(orderData: any): Observable<any> {
    return this.requestorderStock(orderData).pipe(shareReplay(1));
  }

  private requestorderStock({ quantity, companyId, duration = "GTC", orderType = "Market", tradeAction,price, stopPrice }) {
    let data = {
      quantity,
      company_id: companyId,
      duration,
      orderType,
      trade_action: tradeAction,
      price,
      stopPrice,
    }
    return this.httpClient.post(this.urls.orderStock, data).pipe(map(orderStockResponse));
  }

  public getCompanyNewsById(companyId: string, forceCall: boolean = false) {
    if (!this.companyNews$ || !this.companyNews$[companyId] || forceCall) {
      this.companyNews$[companyId] = this.requestCompanyNewsById(companyId).pipe(shareReplay(1));
    }
    return this.companyNews$[companyId];
  }

  private requestCompanyNewsById(companyId: any) {
    var data = {
      "company_id":companyId,
      // "symbol":"AAPL",
      "limit":5,
      "offset":0
    }
    return this.httpClient.post(this.urls.companyNews, data).pipe(map(companyNewsResponse));
  }

    // Pending Order (BUY or SELL)
    public pendingOrder(payload): Observable<any> {
      return this.requestpendingOrder(payload).pipe(shareReplay(1));
    }
  
    private requestpendingOrder(payload) {
      let url = this.urls.pending+`?page=${payload.page}&perPage=${payload.perPage}`;

      return this.httpClient.get(url).pipe(map(pendingOrderResponse));
    }

  // Pre Order (BUY or SELL)
  public preOrderDetail(companyId): Observable<any> {
    return this.requestPreOrderDetail(companyId).pipe(shareReplay(1));
  }

  private requestPreOrderDetail(companyId) {
    let url = this.urls.preOrderDetail.replace("{company_id}", `${companyId}`);
    return this.httpClient.get(url).pipe(map(preOrderDetailResponse));
  }


   // Cancel Order
   public cancelOrder(orderId: string): Observable<any> {
    return this.requestcancelOrder(orderId).pipe(shareReplay(1));
  }

  private requestcancelOrder(orderId: string) {
    let deleteUlr = this.urls.cancelOrder.replace("{order_id}", `${orderId}`);

    return this.httpClient.delete(deleteUlr).pipe(map((res: any) => res));
  }

}
