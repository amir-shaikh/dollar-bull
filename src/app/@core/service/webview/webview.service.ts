import { Injectable } from '@angular/core';
import {registerWebPlugin} from "@capacitor/core";
// import {OAuth2Client} from '@byteowls/capacitor-oauth2';
import { Plugins } from '@capacitor/core';
const { Browser } = Plugins




@Injectable({
  providedIn: 'root'
})
export class WebviewService {

  KYC_URL :string= "https://getstarted2.tradestation.com/intro?offer=DLLRAFVH";

  constructor() { 
    // registerWebPlugin(OAuth2Client);
  }

  
  loginTradeStation(token:string, cb:any){
    //https://sim-api.tradestation.com/v2/authorize/?redirect_uri=https://exampleclientapp.com/authcode.aspx&client_id=3AD19ABE-8B2F-4CD6-BC50-07418E89C266&response_type=code
    // let redirectUrl = "http://localhost/user/v1/thirdparty/login/" + token;
    let redirectUrl = "https://exampleclientapp.com/authcode.aspx";
    let url = "https://sim-api.tradestation.com/v2/authorize/?redirect_uri="+ redirectUrl +"&client_id=3AD19ABE-8B2F-4CD6-BC50-07418E89C266&response_type=code";

    // var ref = window.open(url, "_self", "location=no");

    // //@ts-ignore
    // window.hashchange((e) => {console.log("hansh changesd")})
    // setTimeout(() => {ref.close()}, 1000)

    Browser.addListener('browserPageLoaded', (e) => {
      cb()
    })
    Browser.open({ url: url });
    
  }

  open_kyc_link(){
    Browser.open({ url: this.KYC_URL})
  }
}
