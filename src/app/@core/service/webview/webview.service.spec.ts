import { TestBed } from '@angular/core/testing';

import { WebviewService } from './webview.service';

describe('WebviewService', () => {
  let service: WebviewService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WebviewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
