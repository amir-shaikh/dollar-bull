import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { Router, NavigationEnd, NavigationExtras } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class NavigationService {
  history: any[] = [];

  constructor(private router: Router, private location: Location) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.history.push(event.urlAfterRedirects);
      }
    });
  }

  // go back and clear stack
  back(): void {
    this.history.pop();
    if (this.history.length > 0) {
      this.location.back();
    } else {
      this.router.navigate(['/']);
    }
  }

  // reset history and start new route stack
  stackFirst(commands: any[], extras?: NavigationExtras): void {
    this.history = [];
    this.router.navigate(commands, extras);
  }

  // navigate
  navigate(commands: any[], extras?: NavigationExtras): void {
    this.router.navigate(commands, extras);
  }
}
