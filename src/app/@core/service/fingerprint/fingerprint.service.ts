import { Injectable } from '@angular/core';
import { rejects } from 'assert';

declare global {
  interface Window {
    Fingerprint: any;
  }
}

@Injectable({
  providedIn: 'root',
})
export class FingerprintService {
  isFingerAuthenticated: boolean = false;
  constructor() {}

  isAvailable() {
    if (!window.Fingerprint) {
      return Promise.reject('no fingerprint support available');
    }
    let Fingerprint = window.Fingerprint;
    let optionalParams = {};

    return Promise.resolve().then(() => {
      return new Promise((resolve, reject) => {
        Fingerprint.isAvailable(
          isAvailableSuccess,
          isAvailableError,
          optionalParams
        );

        function isAvailableSuccess(result: any) {
          /*
          result depends on device and os. 
          iPhone X will return 'face' other Android or iOS devices will return 'finger' Android P+ will return 'biometric'
          */
          resolve(1);
        }

        function isAvailableError(error: any) {
          // 'error' will be an object with an error code and message
          alert(error.message);
          reject();
        }
      });
    });
  }

  show(text: string) {
    if (!window.Fingerprint) {
      return Promise.reject('no fingerprint support available');
    }
    let Fingerprint = window.Fingerprint;

    return Promise.resolve().then(() => {
      return new Promise((resolve, reject) => {
        Fingerprint.show(
          {
            description: text,
          },
          showSuccess,
          showError
        );

        function showSuccess(result: any) {
          /*
          result depends on device and os. 
          iPhone X will return 'face' other Android or iOS devices will return 'finger' Android P+ will return 'biometric'
          */
          resolve(1);
        }

        function showError(error: any) {
          // 'error' will be an object with an error code and message
          reject();
        }
      });
    });
  }

  setScreenLockPermission(isEnabled: boolean) {
    window.localStorage['isScreenLockEnabled'] = isEnabled;
  }

  getScreenLockPermission() {
    return window.localStorage['isScreenLockEnabled'];
  }

  setIsFingerAuthenticated(){
    this.isFingerAuthenticated = true;
  }

  getIsFingerAuthenticated(){
    return this.isFingerAuthenticated;
  }
}
