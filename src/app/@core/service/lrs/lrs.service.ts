import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { filter, map, shareReplay, tap } from 'rxjs/operators';
import { lrsParser } from './lrs.parser'
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LrsService {
  private urls = {
    banksMaster: '@UserAPI/banks',
    remitOnline: '@UserAPI/onlinelrsprocess',
    remitOffline: '@UserAPI/offlinelrsprocess'
  };

  constructor(private httpClient: HttpClient) { }

  public getBanks(){
    //@ts-ignore
    return this.httpClient.post(this.urls.banksMaster, {}).pipe(map(res => lrsParser.banksListParser(res.result)));
  }

  public RemitOnline(payload){
    //@ts-ignore
    return this.httpClient.post(this.urls.remitOnline, payload).pipe(map(res => lrsParser.remitOnlineParser(res.result.data)));
  }

  public RemitOffline(payload){
    //@ts-ignore
    return this.httpClient.post(this.urls.remitOffline, payload).pipe(map(res => lrsParser.remitOfflineParser(res.result.data)));
  }

  

  
}
