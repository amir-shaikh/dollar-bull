
const banksListParser = (response: any) => {
    let result = [];
    
    if(!response || !response.length){
        return result
    }

    for(const res of response){
        result.push({id:res['bank_master_id'], bank:res['bank_name']});
    }
    return result;
}

const remitOnlineParser = (response: any) => {
    let result = [];
    
    if(!response || !response.length){
        return result
    }
    result = response.slice(0);
    result.sort(function(a,b) {
        return a.order - b.order;
    });
    return result;
}

const remitOfflineParser = (response: any) => {
    let result = "";
    if(!response || !response.length){
        return result
    }
    result = response[0].pdf_path;
    return result;
}



export const lrsParser = {
    banksListParser,
    remitOnlineParser,
    remitOfflineParser
};