import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, shareReplay } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { NewsArticlesPayloadI, NewsPayloadI } from './news-articles.interface';
import { newsArticlesParser } from './news-articles.parser';

@Injectable({
  providedIn: 'root',
})
export class NewsArticlesService {
  private urls = {
    newsArticles: '@DataAPI/news_articles',
    news: '@DataAPI/news',
  };
  
  // @ts-ignore
  private newsArticlesListCache: Observable<any>;

  constructor(private httpClient: HttpClient) {}

  // get news and articles
  public getNewsArticles(payload: NewsArticlesPayloadI, forceCall=false): Observable<any> {
    if (!this.newsArticlesListCache || forceCall) {
      this.newsArticlesListCache = this.requestNewsArticles(payload).pipe(shareReplay(1));
    }
    return this.newsArticlesListCache;
  }

  private requestNewsArticles(payload: NewsArticlesPayloadI) {
    return this.httpClient.post(this.urls.newsArticles, payload).pipe(map(newsArticlesParser.list));
  }

    // get news
    public getNews(payload: NewsPayloadI): Observable<any> {
      return this.requestNews(payload).pipe(shareReplay(1));;
    }
  
    private requestNews(payload: NewsPayloadI) {
      let url = this.urls.news+`?page=${payload.page}&perPage=${payload.perPage}`;
      if (payload.company_id) {
        url = url+`&company_id=${payload.company_id}`;
      }
      return this.httpClient.get(url).pipe(map(newsArticlesParser.newsList));
    }
}
