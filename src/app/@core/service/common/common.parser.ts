export const masterFilterParser = (response: any) => {
    response = response.result.filterdata;
    
    let list:any[] = []
    
    if(!response){
        return list
    }

    for(const r of response){
        const hasIndex = list.findIndex(item => item.name === r.name);
        
        if (hasIndex === -1) {
            let listItem:any = {}
            
            listItem['name'] = r['name'] ? r['name'] : ''
            listItem['type'] = r['type'] ? r['type'] : ''
            listItem['value'] = r['value'] ? r['value'] : []
            listItem['selectedValue'] = []
            
            list.push(listItem)
        }
    }
    
    return list
}

const stats = (res: any) => {
    let result: any = {};

    if (res && res.result) {
        res = res.result;
    }

    if (!res || !Object.keys(res).length) {
        return result;
    }

    result = {
        TOTAL_AMOUNT_INVESTED: res['TOTAL_AMOUNT_INVESTED'] ? res['TOTAL_AMOUNT_INVESTED'] : '',
        TOTAL_ACTIVE_LENDERS: res['TOTAL_ACTIVE_LENDERS'] ? res['TOTAL_ACTIVE_LENDERS'] : '',
        AVERAGE_RETURN_EARNED: res['AVERAGE_RETURN_EARNED'] ? res['AVERAGE_RETURN_EARNED'] : '',
        RBI_CERTIFIED: res['RBI_CERTIFIED'] ? res['RBI_CERTIFIED'] : '',
    }

    return result;
}

const overviewStat = (res: any) => {
    let result: any = {};

    if (res && res.result) {
        res = res.result;
    }

    if (!res || !Object.keys(res).length) {
        return result;
    }

    result = {
        customer_invested_value: res['customer_invested_value'] ? res['customer_invested_value'] : 0,
        customer_current_value: res['customer_current_value'] ? res['customer_current_value'] : 0,
        profit_loss_value: res['profit_loss_value'] ? res['profit_loss_value'] : 0,
        profit_loss_per: res['profit_loss_per'] ? res['profit_loss_per'] : 0,
    }

    return result;
}
const performance = (response:any) => {
    let result:any = {
        data: [],
        label: [],
    }

    if(!response || !response.length){
        return result
    }

    for(const res of response){    
        result['data'].push(res['portfolio_value'] ? res['portfolio_value'] : '')
        result['label'].push(res['record_date'] ? res['record_date'] : '')
    }
    return result
}

const investmentParser = (response:any) =>{
    let result:any = {
        labels: [],
        data: []
    }

    if(!response){
        return result
    }
    result['labels'] = Object.keys(response);
    result['data'] = Object.values(response);
    return result
}

const stockPriceChartParser = (response: any) => {
    let result = {
        data: [],
        label: []
    }
    
    if(!response || !response.length){
        return result
    }

    for(const res of response){
        result['label'].push(res['price_date'])
        result['data'].push(res['open'])
    }

    return result
}
const topSectorParser = (res: any) => {
    let result: any = {};

    if (res && res.result) {
        res = res.result;
    }

    if (!res || !Object.keys(res).length) {
        return result;
    }
    result = res.map((r: any) => {
        return {
            name: r['sector'] ? r['sector'] : '',
            sectorId: r['sector_id'] ? r['sector_id'] : '',
            img: r['sectoricon'] ? r['sectoricon'] : '',
        };
    });

    return result;
}
export const commonParser = {
    stats,
    overviewStat,
    performance,
    investmentParser,
    stockPriceChartParser,
    topSectorParser,
};