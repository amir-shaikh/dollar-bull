import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { filter, map, shareReplay, tap } from 'rxjs/operators';
import { commonParser, masterFilterParser } from './common.parser'
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CommonService {
  private urls = {
    filterMaster: '@DataAPI/filter',
    stats: '@DataAPI/stats',
    overview: '@UserAPI/portfolio_overview',
    performance: '@UserAPI/performance_chart',
    investmentChart: '@UserAPI/getCustomerInvestmentsPieChart',
    stockPriceChart: '@DataAPI/stock_price_chart',
    topSectorMaster: '@UserAPI/sectors',
    getCustomerLatestStage: '@UserAPI/currentstage',
    updateCustomerStage : '@UserAPI/updatestage'
  };

  // @ts-ignore
  private statsCache: Observable<any>;

  // @ts-ignore
  private filterDataCache$: Observable<any>;

  // @ts-ignore
  private overViewCache: Observable<any>;
  //@ts-ignore
  public selectedFilter$ = {};

  //@ts-ignore
  private performanceCache$: Observable<any>;
  private customerInvestmentCache$: Observable<any>;

  //@ts-ignore
  private stockPriceChartCache$: Observable<any>

  //@ts-ignore
  private topSectorCache$: Observable<any>;

  public isLoaderEnabled$ = new Subject()

  constructor(private httpClient: HttpClient) { }

  private filterData = [
    {
      name: "Sort By",
      type: "single",
      value: [
        "Market Cap",
        "Price- low"
      ]
    },
    {
      name: "Sector",
      type: "multi",
      value: [
        "Basic Material",
        "Consumer Defensive"
      ]
    }
  ]

  //filterInitialCache
  public getFilterMaster(): Observable<any> {
    if (!this.filterDataCache$) {
      this.filterDataCache$ = this.requestFilterMaster()
    }
    return this.filterDataCache$
  }

  public requestFilterMaster() {
    return this.httpClient.get<any>(`${this.urls.filterMaster}`).pipe(map(res => masterFilterParser(res)))
  }

  public updateSelectedFilter(filterKey: any, filterValue: any) {
    this.selectedFilter$[filterKey] = filterValue
    return this.selectedFilter$
  }

  public getOverview(forceCall = false) {
    if (!this.overViewCache || forceCall) {
      this.overViewCache = this.requestOverviewStat().pipe(shareReplay(1));
    }
    return this.overViewCache;
  }

  //Performance
  public getPerformance(payload: any, forceCall: boolean = false): Observable<any> {
    if (!this.performanceCache$ || forceCall) {
      this.performanceCache$ = this.requestPerformance(payload)
    }
    return this.performanceCache$
  }

  private requestPerformance(payload: any) {
    return this.httpClient.post<any>(`${this.urls.performance}`, payload).pipe(map(res => commonParser.performance(res.result)))
  }

  //Investment Chart
  public getCustomerInvestmentChart(payload: any, forceCall: boolean = false): Observable<any> {
    if (!this.customerInvestmentCache$ || forceCall) {
      this.customerInvestmentCache$ = this.requestCustomerInvestmentChart(payload)
    }
    return this.customerInvestmentCache$
  }

  private requestCustomerInvestmentChart(payload: any) {
    return this.httpClient.post<any>(`${this.urls.investmentChart}`, payload).pipe(map(res => commonParser.investmentParser(res.result)))
  }

  public getStockPriceChart(payload: any, forceCall: boolean = false) {
    if (!this.stockPriceChartCache$) {
      this.stockPriceChartCache$ = this.requestStockPriceChart(payload)
    }
    return this.stockPriceChartCache$
  }

  private requestStockPriceChart(payload) {
    return this.httpClient.post<any>(`${this.urls.stockPriceChart}`, payload).pipe(map(res => commonParser.stockPriceChartParser(res.result)))
  }

  public getMasterFilters() {
    return masterFilterParser(this.filterData)
  }

  // get dollarbull stats
  public getDollarbullStats(forceCall = false): Observable<any> {
    if (!this.statsCache || forceCall) {
      this.statsCache = this.requestDollarbullStats().pipe(shareReplay(1));
    }
    return this.statsCache;
  }

  private requestDollarbullStats() {
    return this.httpClient.get(this.urls.stats).pipe(map(commonParser.stats));
  }

  private requestOverviewStat() {
    const data = {
      "customer_id": "1"
    }
    return this.httpClient.post(this.urls.overview, data).pipe(map(commonParser.overviewStat));
  }

  public setLoader(isEnable: boolean) {
    console.log("isEnabled", isEnable);
    this.isLoaderEnabled$.next(isEnable)
  }

  public getTopSectorsMaster(){
    // if (!this.topSectorCache$) {
      
    // }
    this.topSectorCache$ = this.requestTopSector(); //.pipe(shareReplay(1));
    return this.topSectorCache$;
  }

  private requestTopSector(){
    //@ts-ignore
    return this.httpClient.post(this.urls.topSectorMaster, {}).pipe(map(commonParser.topSectorParser));
  }

  public getCustomerLatestStage() {
    return this.httpClient.post(this.urls.getCustomerLatestStage, {});
  }

  public updateCustomerStage(payload: any) {
    return this.httpClient.post(this.urls.updateCustomerStage, payload);
  }

}
