export interface SignupPayloadI {
    email_id: string;
    login_type: string;
    name: string;
}
export interface onEmailOTPSubmitI {
    email_code: string;
}

export interface updateMobileNoI {
    mobile_no: string;
}

export interface onMobileOTPSubmitI {
    otp: string;
}
export interface submitRMCodeI {
    rm_code: string;
}
export interface app_loginI {
    email_id: string;
    login_type: string;
}
export interface onEmailOTPSubmitLoginI {
    email_id: string;
    email_code: string;
}

