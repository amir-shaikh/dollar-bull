import { Injectable } from '@angular/core';
import jwtDecode from 'jwt-decode';

@Injectable({
  providedIn: 'root',
})
export class JwtService {
  getToken(): string {
    return window.localStorage['jwtToken'];
  }

  saveToken(token: string): void {
    window.localStorage['jwtToken'] = token;
  }

  saveRefCode(refCode: string): void {
    window.localStorage['refCode'] = refCode;
  }

  getRefCode(): string {
    return window.localStorage['refCode'];
  }

  destroyToken(): void {
    window.localStorage.removeItem('jwtToken');
  }

  readTokenData(): any {
    const token = this.getToken();
    const payload = jwtDecode(token);
    return payload;
  }
}
