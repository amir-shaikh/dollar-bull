import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../service/auth/auth.service';
import { NavigationService } from '../service/navigation.service';

/**
 * NoAuthGuard helps guarding non-auth routes like login, register, etc.
 * Routes which shall be accessible by non-auth users
 */
@Injectable({
  providedIn: 'root',
})
export class NoAuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private navigationService: NavigationService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (this.authService.isAuthenticated()) {
      if (route.data && route.data.noAuthGuardRedirectUrl) {
        this.navigationService.navigate([route.data.noAuthGuardRedirectUrl]);
      } else {
        this.navigationService.stackFirst(['/splash']);
      }
      return false;
    }
    return true;
  }
}
