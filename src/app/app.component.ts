import { Component, OnInit, } from '@angular/core';
import { Plugins } from '@capacitor/core';


import { FingerprintService } from './@core/service/fingerprint/fingerprint.service'
import { WebviewService } from './@core/service/webview/webview.service'
import {CommonService} from '../app/@core/service/common/common.service'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent  implements OnInit {
  
  title = 'dollar-bull';
  loaderEnabled
  counter=0;
  constructor(private fingerprintService: FingerprintService, private webviewService: WebviewService, private commonService: CommonService) { }

  ngOnInit(){    
    // this.fingerprintService.isAvailable().then(() => {
    //   alert('it is available');

    //   this.fingerprintService.show("Hey suhail, please verify your finger").then(r => {
    //     alert('done')
    //   })
    // }).catch(e => {
    //   alert('not available')
    // })
    this.backButtonHandler();
    this.commonService.isLoaderEnabled$.subscribe(loaderEnabled => {
      this.loaderEnabled = loaderEnabled
    })

  }

  backButtonHandler(){
    const { App } = Plugins;
    App.addListener('backButton', (state) => {
      // state.isActive contains the active state
      if (this.counter == 0) {
        this.counter++;
        setTimeout(() => { this.counter = 0 }, 3000)
      } else {
        App.exitApp();
      }
    });
  }

 
}
