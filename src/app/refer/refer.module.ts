import { SharedModule } from './../@shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReferRoutingModule } from './refer-routing.module';
import { ReferComponent } from './refer.component';
import { ClipboardModule } from 'ngx-clipboard';


@NgModule({
  declarations: [ReferComponent],
  imports: [
    CommonModule,
    ReferRoutingModule,
    SharedModule,
    ClipboardModule
  ]
})
export class ReferModule { }
