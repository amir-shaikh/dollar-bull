import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { JwtService } from '../@core/service/jwt.service';
const { Share } = Plugins;
@Component({
  selector: 'app-refer',
  templateUrl: './refer.component.html',
})
export class ReferComponent implements OnInit {
  @Output()
  onBackClick: EventEmitter<any> = new EventEmitter<any>();
  @Input()
  disableDefaultBack: boolean = false;
  @Output()
  onSearchChange: EventEmitter<string> = new EventEmitter<string>();
  referUrl = "http://dollarbull.com/?ref_code=";
  referMsg = "Use my reference code to join Dollarbull. Join Using ";
  copyLinkText = "Copy invite link";
  searchInput: string = '';
  strings: any = {
    searchPlaceholder: 'Search',
  };
  searchBlock = false;
  constructor(private jwtService: JwtService,) { }

  ngOnInit(): void {
    let refCode = this.jwtService.getRefCode();
    this.referUrl +=  refCode;
    this.referMsg = this.referMsg + this.referUrl;
  }

  handleSearchChange(e: any) {
    this.searchInput = e.target.value;
    console.log(this.searchInput);
    this.searchBlock = true;
    this.onSearchChange.emit(this.searchInput);
    // setTimeout(() => {
    //   this.searchBlock = false;
    // }, 2000);
  }

  handleCopyToClipboard = () => {
    const el = document.createElement('textarea');
    el.value = this.referMsg;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    alert("Coped to clipboard");
  };

  handleWARefer() {
    var win = window.open(`https://wa.me/?text=${encodeURIComponent(this.referMsg)}`, '_blank');
    win.focus();
  }

  RefContentcopied(e) {
    this.copyLinkText = "Copied";
    setTimeout(() => {
      this.copyLinkText= "Copy invite link";
    }, 5000);
  }

  handleFBRefer() {
    var win = window.open(`https://www.facebook.com/sharer/sharer.php?u=${this.referUrl}`)
    win.focus();
  }

  handleTwitterRefer() {
    var win = window.open(`http://twitter.com/share?text=${this.referMsg}&hashtags=dollarbull`)
    win.focus();
  }

  handleInvite() {
    let shareRet = Share.share({
      title: 'Invite',
      text: this.referMsg,
      url: 'http://ionicframework.com/',
      dialogTitle: 'Share by'
    });
  }
}
