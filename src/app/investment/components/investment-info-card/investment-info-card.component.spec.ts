import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestmentInfoCardComponent } from './investment-info-card.component';

describe('InvestmentInfoCardComponent', () => {
  let component: InvestmentInfoCardComponent;
  let fixture: ComponentFixture<InvestmentInfoCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InvestmentInfoCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestmentInfoCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
